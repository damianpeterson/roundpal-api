<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160719194756 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE deals (id INT AUTO_INCREMENT NOT NULL, gang_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_EF39849B9266B5E (gang_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE deal_members (id INT AUTO_INCREMENT NOT NULL, member_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', deal_id INT NOT NULL, value NUMERIC(10, 2) DEFAULT \'0\' NOT NULL, INDEX IDX_A1BA5A777597D3FE (member_id), INDEX IDX_A1BA5A77F60E2305 (deal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gangs (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', name VARCHAR(255) DEFAULT NULL, deal_type VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_CF53F7EABF396750 (id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gang_memberships (id INT AUTO_INCREMENT NOT NULL, member_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', gang_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', value NUMERIC(10, 2) DEFAULT \'0\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_443295E47597D3FE (member_id), INDEX IDX_443295E49266B5E (gang_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE members (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', name VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, token VARCHAR(255) NOT NULL, code VARCHAR(255) DEFAULT NULL, code_requested_at DATETIME DEFAULT NULL, last_signed_in_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_45A0D2FFBF396750 (id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE deals ADD CONSTRAINT FK_EF39849B9266B5E FOREIGN KEY (gang_id) REFERENCES gangs (id)');
        $this->addSql('ALTER TABLE deal_members ADD CONSTRAINT FK_A1BA5A777597D3FE FOREIGN KEY (member_id) REFERENCES members (id)');
        $this->addSql('ALTER TABLE deal_members ADD CONSTRAINT FK_A1BA5A77F60E2305 FOREIGN KEY (deal_id) REFERENCES deals (id)');
        $this->addSql('ALTER TABLE gang_memberships ADD CONSTRAINT FK_443295E47597D3FE FOREIGN KEY (member_id) REFERENCES members (id)');
        $this->addSql('ALTER TABLE gang_memberships ADD CONSTRAINT FK_443295E49266B5E FOREIGN KEY (gang_id) REFERENCES gangs (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE deal_members DROP FOREIGN KEY FK_A1BA5A77F60E2305');
        $this->addSql('ALTER TABLE deals DROP FOREIGN KEY FK_EF39849B9266B5E');
        $this->addSql('ALTER TABLE gang_memberships DROP FOREIGN KEY FK_443295E49266B5E');
        $this->addSql('ALTER TABLE deal_members DROP FOREIGN KEY FK_A1BA5A777597D3FE');
        $this->addSql('ALTER TABLE gang_memberships DROP FOREIGN KEY FK_443295E47597D3FE');
        $this->addSql('DROP TABLE deals');
        $this->addSql('DROP TABLE deal_members');
        $this->addSql('DROP TABLE gangs');
        $this->addSql('DROP TABLE gang_memberships');
        $this->addSql('DROP TABLE members');
    }
}
