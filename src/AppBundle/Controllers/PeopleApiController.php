<?php

namespace AppBundle\Controllers;

use AppBundle\Exceptions\GroupNotFoundException;
use AppBundle\Exceptions\PersonNotFoundException;
use AppBundle\Library\JsonCorsErrorResponse;
use AppBundle\Library\JsonCorsResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\User;

/**
 * Class PeopleApiController
 * @package AppBundle\Controller
 *
 * @Route("/v1")
 */
class PeopleApiController extends Controller
{
    /**
     * Get a paginated list of People
     *
     * @return Response
     *
     * @Route("/people", name="get_people")
     * @Method({"GET"})
     */
    public function getPeople(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (in_array('ROLE_ADMIN', $user->getRoles()) === false) {
            return new JsonCorsErrorResponse('', Response::HTTP_FORBIDDEN);
        }

        $peopleService = $this->container->get('people_service');
        
        $people = $peopleService->getPeople($request->query->get('offset', 0), $request->query->get('limit', 100));

        return new JsonCorsResponse($people, Response::HTTP_OK);
    }

    /**
     * Get a Person's details
     *
     * @return Response
     *
     * @Route("/people/{id}", name="get_person")
     * @Method({"GET"})
     */
    public function getPerson(Request $request, $id)
    {
        /** @var User $user */
        $user = $this->getUser();

        $peopleService = $this->container->get('people_service');

        try {
            $person = $peopleService->getPerson($id);

            if (in_array('ROLE_ADMIN', $user->getRoles()) === false &&
                $user->getUsername() !== $id &&
                $peopleService->canViewDetails($user->getUsername(), $id) === false
            ) {
                throw new PersonNotFoundException();
            }
            return new JsonCorsResponse($person, Response::HTTP_OK);
        } catch (PersonNotFoundException $personNotFoundException) {
            return new JsonCorsErrorResponse($personNotFoundException::MESSAGE, Response::HTTP_NOT_FOUND);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create a new Person (admin only)
     *
     * @return Response
     *
     * @Route("/people", name="create_person")
     * @Method({"POST"})
     */
    public function createPerson(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (in_array('ROLE_ADMIN', $user->getRoles()) === false) {
            return new JsonCorsErrorResponse('', Response::HTTP_FORBIDDEN);
        }

        $peopleService = $this->container->get('people_service');

        $json = json_decode($request->getContent());

        if (empty($json->name) === true || empty($json->email) === true) {
            return new JsonCorsErrorResponse('A name and email are required', Response::HTTP_BAD_REQUEST);
        }
        
        try {
            $person = $peopleService->createPerson($json->name, $json->email);
            return new JsonCorsResponse($person);
        } catch (\InvalidArgumentException $invalidArgumentException) {
            return new JsonCorsErrorResponse($invalidArgumentException->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Alter a Person's name or email
     *
     * @return Response
     *
     * @Route("/people/{id}", name="alter_person")
     * @Method({"PATCH"})
     */
    public function alterPerson(Request $request, $id)
    {
        $user = $this->getUser();
        $peopleService = $this->container->get('people_service');

        $json = json_decode($request->getContent());

        try {
            if ($user->getUsername() !== $id && $peopleService->canViewDetails($user->getUsername(), $id) === false) {
                throw new PersonNotFoundException();
            }
            $peopleService->alterPerson($id, $json->path, $json->value);
            return new Response('');
        } catch (PersonNotFoundException $personNotFoundException) {
            return new JsonCorsErrorResponse($personNotFoundException::MESSAGE, Response::HTTP_NOT_FOUND);
        } catch (\InvalidArgumentException $invalidArgumentException) {
            return new JsonCorsErrorResponse($invalidArgumentException->getMessage(), Response::HTTP_BAD_REQUEST, $json->path);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $json->path);
        }
    }

    /**
     * Get People for a Group
     *
     * @return Response
     *
     * @Route("/groups/{id}/people", name="get_people_for_group")
     * @Method({"GET"})
     */
    public function getPeopleForGroup($id)
    {
        $user = $this->getUser();
        $groupsService = $this->container->get('groups_service');

        try {
            if (in_array('ROLE_ADMIN', $user->getRoles()) === false &&
                $groupsService->canViewDetails($user->getUsername(), $id) === false
            ) {
                throw new GroupNotFoundException();
            }
            $people = $groupsService->getPeople($id);
            return new JsonCorsResponse($people);
        } catch (GroupNotFoundException $groupNotFoundException) {
            return new JsonCorsErrorResponse($groupNotFoundException::MESSAGE, Response::HTTP_NOT_FOUND);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create a new Person for a Group
     *
     * @return Response
     *
     * @Route("/groups/{id}/people", name="create_person_for_group")
     * @Method({"POST"})
     */
    public function createPersonForGroup(Request $request, $id)
    {
        /** @var User $user */
        $user = $this->getUser();

        $groupsService = $this->container->get('groups_service');

        $json = json_decode($request->getContent());

        // TODO: need to add existing person to a group
        $name = $json->name;
        $email = isset($json->email) ? $json->email : null;

        try {
            if ($groupsService->containsPerson($id, $user->getUsername()) === false) {
                throw new GroupNotFoundException();
            }
            $newPerson = $groupsService->createPerson($id, $name, $email);
            return new JsonCorsResponse($newPerson);
        } catch (GroupNotFoundException $groupNotFoundException) {
            return new JsonCorsErrorResponse(GroupNotFoundException::MESSAGE, Response::HTTP_NOT_FOUND);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Delete a Person from a Group
     *
     * @return Response
     *
     * @Route("/groups/{groupId}/people/{personId}", name="delete_person_from_group")
     * @Method({"DELETE"})
     */
    public function deletePersonFromGroup($groupId, $personId)
    {
        /** @var User $user */
        $user = $this->getUser();

        $groupsService = $this->container->get('groups_service');
        $peopleService = $this->container->get('people_service');

        try {
            if ($groupsService->containsPerson($groupId, $user->getUsername()) === false) {
                throw new GroupNotFoundException();
            }
            if ($user->getUsername() !== $personId && $peopleService->canViewDetails($user->getUsername(), $personId) === false) {
                throw new PersonNotFoundException();
            }
            $groupsService->removePerson($groupId, $personId);
            return new JsonCorsResponse('');
        } catch (GroupNotFoundException $groupNotFoundException) {
            return new JsonCorsErrorResponse(GroupNotFoundException::MESSAGE, Response::HTTP_NOT_FOUND);
        } catch (PersonNotFoundException $personNotFoundException) {
            return new JsonCorsErrorResponse(PersonNotFoundException::MESSAGE, Response::HTTP_NOT_FOUND);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
