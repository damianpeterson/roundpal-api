<?php

namespace AppBundle\Controllers;

use AppBundle\Exceptions\GroupNotFoundException;
use AppBundle\Exceptions\InvalidTransactionException;
use AppBundle\Exceptions\PersonNotFoundException;
use AppBundle\Library\JsonCorsErrorResponse;
use AppBundle\Library\JsonCorsResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\User;

/**
 * Class TransactionsApiController
 * @package AppBundle\Controller
 *
 * @Route("/v1")
 */
class TransactionsApiController extends Controller
{
    /**
     * Get Transactions for Group
     *
     * @return Response
     *
     * @Route("/groups/{id}/transactions", name="get_transaction")
     * @Method({"GET"})
     */
    public function getTransactionsForGroup(Request $request, $id)
    {
        /** @var User $user */
        $user = $this->getUser();

        $groupsService = $this->container->get('groups_service');
        $transactionsService = $this->container->get('transactions_service');

        try {
            if (in_array('ROLE_ADMIN', $user->getRoles()) === false &&
                $groupsService->canViewDetails($user->getUsername(), $id) === false
            ) {
                throw new GroupNotFoundException();
            }
            $transactions = $transactionsService->getTransactionsForGroup($id, $request->query->get('offset', 0), $request->query->get('limit', 100));
            return new JsonCorsResponse($transactions);
        } catch (GroupNotFoundException $groupNotFoundException) {
            return new JsonCorsErrorResponse($groupNotFoundException::MESSAGE, Response::HTTP_NOT_FOUND);
        } catch (PersonNotFoundException $personNotFoundException) {
            return new JsonCorsErrorResponse($personNotFoundException::MESSAGE, Response::HTTP_NOT_FOUND);
        } catch (InvalidTransactionException $invalidTransaction) {
            return new JsonCorsErrorResponse('Invalid transaction', Response::HTTP_BAD_REQUEST);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create a new Transaction for a Group
     *
     * @return Response
     *
     * @Route("/groups/{id}/transactions", name="create_transaction")
     * @Method({"POST"})
     */
    public function createTransaction(Request $request, $id)
    {
        /** @var User $user */
        $user = $this->getUser();

        $groupsService = $this->container->get('groups_service');
        $transactionsService = $this->container->get('transactions_service');

        $json = json_decode($request->getContent(), true);

        try {
            if (in_array('ROLE_ADMIN', $user->getRoles()) === false &&
                $groupsService->canViewDetails($user->getUsername(), $id) === false
            ) {
                throw new GroupNotFoundException();
            }
            $transaction = $transactionsService->createTransaction($id, $json['people']);
            return new JsonCorsResponse($transaction);
        } catch (GroupNotFoundException $groupNotFoundException) {
            return new JsonCorsErrorResponse($groupNotFoundException::MESSAGE, Response::HTTP_NOT_FOUND);
        } catch (PersonNotFoundException $personNotFoundException) {
            return new JsonCorsErrorResponse($personNotFoundException::MESSAGE, Response::HTTP_NOT_FOUND);
        } catch (InvalidTransactionException $invalidTransaction) {
            return new JsonCorsErrorResponse('Invalid transaction: '.$invalidTransaction->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
