<?php

namespace AppBundle\Controllers;

use AppBundle\Exceptions\ExpiredCodeException;
use AppBundle\Exceptions\PersonNotFoundException;
use AppBundle\Library\JsonCorsErrorResponse;
use AppBundle\Library\JsonCorsResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SecurityApiController
 * @package AppBundle\Controller
 *
 * @Route("/v1")
 */
class SecurityApiController extends Controller
{
    /**
     * Sign up with an email address and optional name
     * If a Person exists with the provided email address a new code will be generated and an email sent to sign in
     * If not, this creates an account for an email address and sends an email with a sign in code
     *
     * @return JsonCorsResponse
     *
     * @Route("/sign-up-in", name="sign_up_in")
     * @Method({"POST"})
     */
    public function signUpIn(Request $request)
    {
        $peopleService = $this->container->get('people_service');

        $json = json_decode($request->getContent());
        
        if (empty($json->email)) {
            return new JsonCorsErrorResponse('Please provide an email address', Response::HTTP_BAD_REQUEST);
        }

        $email = $json->email;
        
        if ($email === null) {
            return new JsonCorsErrorResponse('Unable to find person', Response::HTTP_NOT_FOUND);
        }

        try {
            $existingPerson = $peopleService->getPersonByEmail($email);
            $code = $peopleService->createRequestCode($existingPerson['data']['id']);

            return new JsonCorsResponse($existingPerson, Response::HTTP_OK);
        } catch (PersonNotFoundException $personNotFoundException) {
            // it's ok, just have to create a new person below
        }

        if (empty($json->name)) {
            $emailParts = explode('@', $email);
            $name = ucwords($emailParts[0]);
        } else {
            $name = $json->name;
        }

        try {
            $person = $peopleService->createPerson($name, $email);
            $group = $peopleService->createGroup($person['data']['id'], 'My Group');
            $code = $peopleService->createRequestCode($person['data']['id']);

            return new JsonCorsResponse($person, Response::HTTP_CREATED);
        } catch (\InvalidArgumentException $invalidArgumentException) {
            return new JsonCorsErrorResponse($invalidArgumentException->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    /**
     * Sign in with a code received by email
     *
     * @return Response
     *
     * @Route("/sign-in/{id}/{code}", name="sign_in")
     * @Method({"GET"})
     */
    public function signIn(Request $request, $id, $code)
    {
        $peopleService = $this->container->get('people_service');

        try {
            $token = $peopleService->getTokenForCode($id, $code, true);
            return new Response($token, 200);
        } catch (PersonNotFoundException $personNotFoundException) {
            return new JsonCorsErrorResponse('Unable to find person', Response::HTTP_NOT_FOUND);
        } catch (ExpiredCodeException $expiredCodeException) {
            return new JsonCorsErrorResponse('That code is expired or not valid', Response::HTTP_UNAUTHORIZED);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse('', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
