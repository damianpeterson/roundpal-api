<?php

namespace AppBundle\Controllers;

use AppBundle\Exceptions\GroupNotFoundException;
use AppBundle\Exceptions\PersonNotFoundException;
use AppBundle\Library\JsonCorsErrorResponse;
use AppBundle\Library\JsonCorsResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\User;

/**
 * Class GroupsApiController
 * @package AppBundle\Controller
 *
 * @Route("/v1")
 */
class GroupsApiController extends Controller
{
    /**
     * Get a paginated list of Groups
     *
     * @return Response
     *
     * @Route("/groups", name="get_groups")
     * @Method({"GET"})
     */
    public function getGroups(Request $request)
    {
        if (in_array('ROLE_ADMIN', $this->getUser()->getRoles()) === false) {
            return new JsonCorsErrorResponse('', Response::HTTP_FORBIDDEN);
        }

        $groupsService = $this->container->get('groups_service');

        $groups = $groupsService->getGroups($request->query->get('offset', 0), $request->query->get('limit', 100));

        return new JsonCorsResponse($groups);
    }

    /**
     * Get a Groups's details
     *
     * @return Response
     *
     * @Route("/groups/{id}", name="get_group")
     * @Method({"GET"})
     */
    public function getGroup($id)
    {
        /** @var User $user */
        $user = $this->getUser();

        $groupsService = $this->container->get('groups_service');

        try {
            $group = $groupsService->getGroup($id);

            // if they're not an admin and they don't belong to the group throw an error
            if (in_array('ROLE_ADMIN', $user->getRoles()) === false &&
                $groupsService->canViewDetails($user->getUsername(), $id) === false
            ) {
                throw new GroupNotFoundException();
            }
            return new JsonCorsResponse($group);
        } catch (GroupNotFoundException $groupNotFoundException) {
            return new JsonCorsErrorResponse($groupNotFoundException::MESSAGE, Response::HTTP_NOT_FOUND);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create a new Group
     *
     * @return Response
     *
     * @Route("/groups", name="create_group")
     * @Method({"POST"})
     */
    public function createGroup(Request $request)
    {
        if (in_array('ROLE_ADMIN', $this->getUser()->getRoles()) === false) {
            return new JsonCorsErrorResponse('Only admins can create groups', Response::HTTP_FORBIDDEN);
        }

        $groupsService = $this->container->get('groups_service');

        $json = json_decode($request->getContent());
        
        if (empty($json->name) === true || empty($json->type) === true) {
            return new JsonCorsErrorResponse('A name and email are required', Response::HTTP_BAD_REQUEST);
        }

        try {
            $group = $groupsService->createGroup(
                $json->name,
                $json->type
            );
            return new JsonCorsResponse($group);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Alter a Group's name
     *
     * @return Response
     *
     * @Route("/groups/{id}", name="alter_group")
     * @Method({"PATCH"})
     */
    public function alterGroup(Request $request, $id)
    {
        /** @var User $user */
        $user = $this->getUser();

        $groupsService = $this->container->get('groups_service');

        $json = json_decode($request->getContent());

        try {
            // if they're not an admin and they don't below to the group throw an error
            if (in_array('ROLE_ADMIN', $user->getRoles()) === false &&
                $groupsService->canViewDetails($user->getUsername(), $id) === false
            ) {
                throw new GroupNotFoundException();
            }
            $groupsService->alterGroup($id, $json->path, $json->value);
            return new Response('');
        } catch (GroupNotFoundException $groupNotFoundException) {
            return new JsonCorsErrorResponse($groupNotFoundException::MESSAGE, Response::HTTP_NOT_FOUND);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Switch the Group's type from money to simple or from simple to money using an optional factor to alter each
     * Person's values
     *
     * @return Response
     *
     * @Route("/groups/{id}/switch", name="switch_group")
     * @Method({"POST"})
     */
    public function switchGroupType(Request $request, $id)
    {
        $groupsService = $this->container->get('groups_service');

        $json = json_decode($request->getContent());

        $type = $json->type;
        $factor = isset($json->factor) ? $json->factor : 0.00;

        try {
            $group = $groupsService->switchType($id, $type, $factor);
            return new JsonCorsResponse($group);
        } catch (\InvalidArgumentException $invalidArgumentException) {
            return new JsonCorsErrorResponse('Invalid type supplied. Must be simple or money.', Response::HTTP_BAD_REQUEST);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get Groups for a Person
     *
     * @return Response
     *
     * @Route("/people/{id}/groups", name="get_groups_for_person")
     * @Method({"GET"})
     */
    public function getGroupsForPerson(Request $request, $id)
    {
        /** @var User $user */
        $user = $this->getUser();

        $peopleService = $this->container->get('people_service');

        try {
            // if they're not an admin and they don't belong to the group throw an error
            if (in_array('ROLE_ADMIN', $user->getRoles()) === false &&
                $peopleService->canViewDetails($user->getUsername(), $id) === false
            ) {
                throw new PersonNotFoundException();
            }
            $groups = $peopleService->getGroups($id);
            return new JsonCorsResponse($groups);
        } catch (PersonNotFoundException $personNotFoundException) {
            return new JsonCorsErrorResponse($personNotFoundException::MESSAGE, Response::HTTP_NOT_FOUND);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create a new Group for a Person
     *
     * @return Response
     *
     * @Route("/people/{id}/groups", name="create_group_for_person")
     * @Method({"POST"})
     */
    public function createGroupForPerson(Request $request, $id)
    {
        $peopleService = $this->container->get('people_service');

        $json = json_decode($request->getContent());

        try {
            $group = $peopleService->createGroup($id, $json->name, $json->type);
            return new JsonCorsResponse($group);
        } catch (\Exception $exception) {
            return new JsonCorsErrorResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
