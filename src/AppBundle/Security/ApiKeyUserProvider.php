<?php

namespace AppBundle\Security;

use AppBundle\Exceptions\PersonNotFoundException;
use AppBundle\Services\PeopleService;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class ApiKeyUserProvider implements UserProviderInterface
{
    protected $peopleService;
    protected $admins;

    public function __construct(PeopleService $peopleService, $admins)
    {
        if (isset($admins) === false) {
            $admins = [];
        }

        $this->peopleService = $peopleService;
        $this->admins = $admins;
    }
    
    public function getUsernameForApiKey($apiKey)
    {
        try {
            $person = $this->peopleService->getPersonByApiKey($apiKey);
            $username = @$person['data']['id'];
        } catch (PersonNotFoundException $personNotFoundException) {
            $username = null;
        }

        return $username;
    }

    public function loadUserByUsername($username)
    {
        $roles = ['ROLE_API'];

        if (in_array($username, $this->admins) === true) {
            $roles[] = 'ROLE_ADMIN';
        }

        return new User(
            $username,
            null,
            $roles
        );
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return 'Symfony\Component\Security\Core\User\User' === $class;
    }
}
