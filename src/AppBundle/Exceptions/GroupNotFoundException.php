<?php

namespace AppBundle\Exceptions;

class GroupNotFoundException extends \Exception
{
    const MESSAGE = 'Unable to find group';
}
