<?php

namespace AppBundle\Exceptions;

class GroupMembershipNotFoundException extends \Exception
{
    const MESSAGE = 'Unable to find group membership';
}
