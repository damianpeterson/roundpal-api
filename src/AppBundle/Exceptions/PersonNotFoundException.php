<?php

namespace AppBundle\Exceptions;

class PersonNotFoundException extends \Exception
{
    const MESSAGE = 'Unable to find person';
}
