<?php

namespace AppBundle\Library;

use AppBundle\Transformers\ErrorTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;

class JsonCorsErrorResponse extends JsonCorsResponse
{
    /**
     * @param mixed $message The response message
     * @param int   $status  The response status code
     */
    public function __construct($message, $status, $field = null)
    {
        $fractal = new Manager();
        $error = new Item(['code' => $status, 'message' => $message, 'field' => $field], new ErrorTransformer());
        $data = $fractal->createData($error)->toArray();
        
        parent::__construct($data, $status);
    }
}
