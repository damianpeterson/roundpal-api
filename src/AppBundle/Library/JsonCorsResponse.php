<?php

namespace AppBundle\Library;

use Symfony\Component\HttpFoundation\JsonResponse;

class JsonCorsResponse extends JsonResponse
{
    /**
     * @param mixed $data    The response data
     * @param int   $status  The response status code
     */
    public function __construct($data = null, $status = 200, $headers = [], $json = false)
    {
        $headers = array_merge($headers, [
            'Access-Control-Allow-Origin'  => '*',
            'Access-Control-Allow-Headers' => 'X-Api-Key, Origin, X-Requested-With, Content-Type, Accept'
        ]);
        
        parent::__construct($data, $status, $headers, $json);
    }
}
