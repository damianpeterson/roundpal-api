<?php

namespace AppBundle\Services;

use AppBundle\Entity\Gang;
use AppBundle\Entity\GangMembership;
use AppBundle\Entity\Member;
use AppBundle\Exceptions\ExpiredCodeException;
use AppBundle\Exceptions\PersonNotFoundException;
use AppBundle\Transformers\GangTransformer;
use AppBundle\Transformers\MemberTransformer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use Monolog\Logger;

class PeopleService
{
    protected $entityManager;
    protected $inviteService;
    protected $maxCodeAge;
    protected $fractal;
    protected $logger;

    public function __construct(EntityManager $entityManager, InviteService $inviteService, $maxCodeAge, Logger $logger)
    {
        $this->entityManager = $entityManager;
        $this->inviteService = $inviteService;
        $this->maxCodeAge = $maxCodeAge;
        $this->fractal = new Manager();
        $this->logger = $logger;
    }

    /**
     * Get a Person by their API key
     *
     * @param $apiKey
     * @return array
     * @throws PersonNotFoundException
     */
    public function getPersonByApiKey($apiKey)
    {
        $memberRepository = $this->entityManager->getRepository('AppBundle:Member');
        $member = $memberRepository->findOneBy(['token' => $apiKey]);
        
        if (empty($member) === true) {
            throw new PersonNotFoundException();
        }

        $person = new Item($member, new MemberTransformer);
        return $this->fractal->createData($person)->toArray();
    }

    /**
     * Get a Person by their email address.
     * This is used for the signUpIn process
     *
     * @param $email
     * @return array
     * @throws PersonNotFoundException
     */
    public function getPersonByEmail($email)
    {
        $memberRepository = $this->entityManager->getRepository('AppBundle:Member');
        $member = $memberRepository->findOneBy(['email' => $email]);

        if (empty($member) === true) {
            throw new PersonNotFoundException();
        }

        $person = new Item($member, new MemberTransformer);
        return $this->fractal->createData($person)->toArray();
    }

    /**
     * Get a token for a Person by their code
     *
     * @param $id
     * @param $code
     * @param bool $clearCode
     * @return array
     * @throws ExpiredCodeException
     * @throws PersonNotFoundException
     */
    public function getTokenForCode($id, $code, $clearCode = false)
    {
        $member = $this->entityManager->find('AppBundle:Member', $id);

        if (empty($member) === true) {
            throw new PersonNotFoundException();
        }

        if ($member->getCode() === null || $member->getCode() !== $code || $member->getCodeRequestedAt() === null) {
            throw new ExpiredCodeException();
        }

        $now = new \DateTime('now');
        $codeAge = $now->getTimestamp() - $member->getCodeRequestedAt()->getTimestamp();
        if (abs($codeAge) > $this->maxCodeAge) {
            throw new ExpiredCodeException();
        }

        if ($clearCode === true) {
            $member->setLastSignedInAt(new \DateTime('now'));
            $member->setUpdatedAt(new \DateTime('now'));
            $member->setCode(null);
            $member->setCodeRequestedAt(null);
            $this->entityManager->persist($member);
            $this->entityManager->flush();
        }

        return $member->getToken();
    }

    /**
     * Generate a new request code for someone to log in with.
     * This is use to send a Person an email with a link they can click to be given their token for future calls.
     * It regenerates the Person's token and takes note of the time the code was requested so that the login attempt
     * can be denied if too much time has elapsed since the request.
     *
     * @param $id
     * @return string
     * @throws PersonNotFoundException
     */
    public function createRequestCode($id)
    {
        /** @var Member $member */
        $member = $this->entityManager->find('AppBundle:Member', $id);

        if (empty($member) === true) {
            throw new PersonNotFoundException();
        }

        $code = (string)mt_rand(1000, 9999);
        $member->setCode($code);

        $this->inviteService->sendCode($member->getId());

        $member->setCodeRequestedAt(new \DateTime('now'));
        $member->setUpdatedAt(new \DateTime('now'));

        $this->entityManager->persist($member);
        $this->entityManager->flush();

        return $code;
    }

    public function resetToken($id)
    {
        $member = $this->entityManager->find('AppBundle:Member', $id);

        if (empty($member) === true) {
            throw new PersonNotFoundException();
        }

        $member->setCode(null);

        $token = bin2hex(random_bytes(16));
        $member->setToken($token);

        $member->setCodeRequestedAt(null);
        $member->setUpdatedAt(new \DateTime('now'));

        $this->entityManager->persist($member);
        $this->entityManager->flush();
    }

    /**
     * Get all People
     *
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function getPeople($offset = 0, $limit = 100)
    {
        if ($offset === null || is_numeric($offset) === false || $limit < 0) {
            $offset = 0;
        }
        if ($limit === null || is_numeric($limit) === false || $limit > 1000 || $limit < 1) {
            $limit = 100;
        }
        
        $memberRepository = $this->entityManager->getRepository('AppBundle:Member');
        $members = $memberRepository->findBy([], ['createdAt' => 'ASC', 'name' => 'ASC'], $limit, $offset);

        $people = new Collection($members, new MemberTransformer);
        //$cursor = new Cursor($offset, null, $offset + $limit, $limit);
        //$people->setCursor($cursor);
        return $this->fractal->createData($people)->toArray();
    }

    /**
     * Get a Person's details
     *
     * @param $id
     * @return array
     * @throws PersonNotFoundException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getPerson($id)
    {
        $member = $this->entityManager->find('AppBundle:Member', $id);

        if (empty($member) === true) {
            throw new PersonNotFoundException();
        }

        $person = new Item($member, new MemberTransformer);
        return $this->fractal->createData($person)->toArray();
    }

    /**
     * Create a new Person
     * In this context a Person requires a name and email. The GroupsService provides a way to create People with
     * just a name.
     *
     * @param $name
     * @param $email
     * @param null $id
     * @return array
     */
    public function createPerson($name, $email)
    {
        if (!preg_match('/^.+\@\S+\.\S+$/', $email)) {
            throw new \InvalidArgumentException('You must supply a valid email address');
        }

        $existingMember = $this->entityManager->getRepository('AppBundle:Member')->findBy(['email' => $email], [], 1);
        
        if ($existingMember) {
            throw new \InvalidArgumentException('A person with this email address already exists');
        }
        
        $member = new Member();
        $member->setName($name);
        $member->setEmail($email);
        $member->setToken(bin2hex(random_bytes(16)));
        $member->setCreatedAt(new \DateTime('now'));
        $member->setUpdatedAt(new \DateTime('now'));
        
        $this->entityManager->persist($member);
        $this->entityManager->flush();
        
        $person = new Item($member, new MemberTransformer);
        return $this->fractal->createData($person)->toArray();
    }

    /**
     * Get an array of Groups a Person belongs to
     *
     * @param $id
     * @return array
     * @throws PersonNotFoundException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getGroups($id)
    {
        $member = $this->entityManager->find('AppBundle:Member', $id);

        if (empty($member) === true) {
            throw new PersonNotFoundException();
        }
        
        $gangRepository = $this->entityManager->getRepository('AppBundle:Gang');
        
        $gangs = $gangRepository->getGangsForMember($member);
        $groups = new Collection($gangs, new GangTransformer);
        
        return $this->fractal->createData($groups)->toArray();
    }

    /**
     * Alter a Person's name or email address
     *
     * @param $id
     * @param $field
     * @param $value
     * @return bool
     * @throws PersonNotFoundException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function alterPerson($id, $field, $value)
    {
        $memberRepository = $this->entityManager->getRepository('AppBundle:Member');

        /** @var Member $member */
        $member = $memberRepository->find($id);

        if (empty($member) === true) {
            throw new PersonNotFoundException();
        }
        
        switch ($field) {
            case 'email':
                $existingEmailMembers = [];

                if (empty($value) === false) {
                    /** @var Member[] $existingEmailMembers */
                    $existingEmailMembers = $memberRepository->findBy(['email' => $value]);
                }

                if (count($existingEmailMembers) > 1 ||
                    (count($existingEmailMembers) === 1 && $existingEmailMembers[0]->getId() !== $id)) {
                    throw new \InvalidArgumentException('This email address is already used by someone else');
                }

                $member->setEmail($value);
                break;
            case 'name':
                $member->setName($value);
                break;
            default:
                throw new \InvalidArgumentException();
                break;
        }
        
        $member->setUpdatedAt(new \DateTime('now'));
        
        $this->entityManager->persist($member);
        $this->entityManager->flush();
        
        return true;
    }

    /**
     * Create a Group for a Person and give them membership to it.
     *
     * @param $personId
     * @param $name
     * @param $type
     * @return array
     * @throws PersonNotFoundException
     */
    public function createGroup($personId, $name, $type = Gang::TYPE_SIMPLE)
    {
        $member = $this->entityManager->find('AppBundle:Member', $personId);

        if (empty($member) === true) {
            throw new PersonNotFoundException();
        }
        
        if (Gang::isValidDealType($type) === false) {
            throw new \InvalidArgumentException(sprintf("%s is not a valid group type", $type));
        }
        
        $newGang = new Gang();
        $newGang->setName($name);
        $newGang->setDealType($type);
        $newGang->setCreatedAt(new \DateTime('now'));
        $newGang->setUpdatedAt(new \DateTime('now'));
        
        $newGangMembership = new GangMembership();
        $newGangMembership->setMember($member);
        $newGangMembership->setGang($newGang);
        $newGangMembership->setCreatedAt(new \DateTime('now'));
        $newGangMembership->setUpdatedAt(new \DateTime('now'));
        $newGangMembership->setValue(0.00);
        
        $newGang->addGangMembership($newGangMembership);

        $this->entityManager->persist($newGangMembership);
        $this->entityManager->persist($newGang);
        $this->entityManager->flush();
        
        $group = new Item($newGang, new GangTransformer);
        return $this->fractal->createData($group)->toArray();
    }

    /**
     * Check whether one Person can view another Person's details.
     * In order for this to be true they must both be in the same Group.
     *
     * @param $personId
     * @param $targetPersonId
     * @return bool
     */
    public function canViewDetails($personId, $targetPersonId)
    {
        $gangMembershipRepository = $this->entityManager->getRepository('AppBundle:GangMembership');

        $result = $gangMembershipRepository->bothInSameGang($personId, $targetPersonId);
        return $result;
    }
}
