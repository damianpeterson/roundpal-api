<?php

namespace AppBundle\Services;

use AppBundle\Entity\Deal;
use AppBundle\Entity\DealMember;
use AppBundle\Entity\Gang;
use AppBundle\Entity\Member;
use AppBundle\Exceptions\GroupNotFoundException;
use AppBundle\Exceptions\InvalidTransactionException;
use AppBundle\Exceptions\PersonNotFoundException;
use AppBundle\Transformers\DealTransformer;
use Doctrine\ORM\EntityManagerInterface;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class TransactionsService
{
    protected $entityManager;
    protected $fractal;
    
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->fractal = new Manager();
    }

    /**
     * @param string $id The ID of the Group
     * @param int $offset
     * @param int $limit
     * @return array
     * @throws GroupNotFoundException
     */
    public function getTransactionsForGroup($id, $offset = 0, $limit = 100)
    {
        $gang = $this->entityManager->find('AppBundle:Gang', $id);
        
        if (empty($gang)) {
            throw new GroupNotFoundException();
        }
        
        $dealRepository = $this->entityManager->getRepository('AppBundle:Deal');
        
        $gangDeals = $dealRepository->getDealsForGang($gang, $offset, $limit);
        $transactions = new Collection($gangDeals, new DealTransformer());

        return $this->fractal->createData($transactions)->toArray();
    }

    public function createTransaction($groupId, array $people = [])
    {
        /** @var Gang $gang */
        $gang = $this->entityManager->find('AppBundle:Gang', $groupId);

        if (empty($gang)) {
            throw new GroupNotFoundException();
        }

        $zeroSum = 0;
        $uniqueIds = [];

        $deal = new Deal();
        $deal->setCreatedAt(new \DateTime('now'));
        $deal->setUpdatedAt(new \DateTime('now'));
        $deal->setGang($gang);

        foreach ($people as $person) {
            if (array_key_exists('id', $person) === false ||
                array_key_exists('value', $person) === false ||
                is_numeric($person['value']) === false
            ) {
                throw new InvalidTransactionException('Missing id or value or the value is not a number');
            }

            $uniqueIds[$person['id']] = true;
            
            $value = round((float)$person['value'], 2);
            if ($gang->getDealType() === Gang::TYPE_SIMPLE) {
                $value = (int)$person['value'];
            }

            /** @var Member $member */
            $member = $this->entityManager->find('AppBundle:Member', $person['id']);
            
            if (empty($member)) {
                throw new PersonNotFoundException();
            }

            $dealMember = new DealMember();
            $dealMember->setDeal($deal);
            $dealMember->setMember($member);
            $dealMember->setValue($value);
            
            $deal->addDealMember($dealMember);
            
            $gangMembership = $member->getGangMembership($gang);
            
            if (empty($gangMembership)) {
                throw new InvalidTransactionException(
                    sprintf('Person %s does not belong to Group %s', $member->getId(), $gang->getId())
                );
            }
            
            $gangMembership->setValue($gangMembership->getValue() + $value);
            
            $this->entityManager->persist($gangMembership);
            $this->entityManager->persist($dealMember);
            
            $zeroSum += $value;
        }

        if (round($zeroSum, 2) !== 0.00) {
            throw new InvalidTransactionException('The values supplied do not add up to zero');
        }

        if (count($uniqueIds) !== count($people)) {
            throw new InvalidTransactionException('Each person must be unique');
        }
        
        $this->entityManager->persist($deal);
        $this->entityManager->flush();
        
        $transaction = new Item($deal, new DealTransformer());

        return $this->fractal->createData($transaction)->toArray();
    }
}
