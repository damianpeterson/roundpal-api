<?php

namespace AppBundle\Services;

use AppBundle\Entity\Gang;
use AppBundle\Entity\GangMembership;
use AppBundle\Entity\Member;
use AppBundle\Exceptions\GroupMembershipNotFoundException;
use AppBundle\Exceptions\GroupNotFoundException;
use AppBundle\Exceptions\PersonNotFoundException;
use AppBundle\Exceptions\RecalculationException;
use AppBundle\Transformers\GangMemberTransformer;
use AppBundle\Transformers\GangTransformer;
use AppBundle\Transformers\MemberTransformer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class GroupsService
{
    protected $entityManager;
    protected $inviteService;
    protected $fractal;

    public function __construct(EntityManager $entityManager, InviteService $inviteService)
    {
        $this->entityManager = $entityManager;
        $this->inviteService = $inviteService;
        $this->fractal = new Manager();
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function getGroups($offset = 0, $limit = 100)
    {
        if ($offset === null || is_numeric($offset) === false || $limit < 0) {
            $offset = 0;
        }
        if ($limit === null || is_numeric($limit) === false || $limit > 1000 || $limit < 1) {
            $limit = 100;
        }
        
        $gangRepository = $this->entityManager->getRepository('AppBundle:Gang');
        $gangs = $gangRepository->findBy([], ['createdAt' => 'ASC', 'name' => 'ASC'], $limit, $offset);

        $groups = new Collection($gangs, new GangTransformer());
        return $this->fractal->createData($groups)->toArray();
    }

    /**
     * @param $id
     * @return array
     * @throws GroupNotFoundException
     */
    public function getGroup($id)
    {
        $gang = $this->entityManager->find('AppBundle:Gang', $id);

        if (empty($gang) === true) {
            throw new GroupNotFoundException();
        }

        $group = new Item($gang, new GangTransformer());
        return $this->fractal->createData($group)->toArray();
    }

    /**
     * Gets People who belong to a Group and includes their current value relative to that Group
     *
     * @param $id
     * @return array
     * @throws GroupNotFoundException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getPeople($id)
    {
        $gang = $this->entityManager->find('AppBundle:Gang', $id);

        if (empty($gang) === true) {
            throw new GroupNotFoundException();
        }
        
        $gangMembershipRepository = $this->entityManager->getRepository('AppBundle:GangMembership');
        
        /** @var GangMembership[] | ArrayCollection $gangMemberships */
        $gangMemberships = $gangMembershipRepository->getGangMembershipsForGang($gang);
        
        $people = new Collection($gangMemberships, new GangMemberTransformer);
        return $this->fractal->createData($people)->toArray();
    }

    /**
     * Get whether this Group contains a Person with this ID
     *
     * @param $id
     * @param $personId
     * @return bool
     */
    public function containsPerson($id, $personId)
    {
        $people = $this->getPeople($id);

        foreach ($people['data'] as $person) {
            if ($person['id'] === $personId) {
                return true;
            }
        }

        return false;
    }

    /**
     * Create a new Group
     *
     * @param $name
     * @param $type
     * @param null $id
     * @return array
     */
    public function createGroup($name, $type)
    {
        if (Gang::isValidDealType($type) === false) {
            throw new \InvalidArgumentException(sprintf("%s is not a valid group type", $type));
        }
        $gang = new Gang();
        $gang->setName($name);
        $gang->setDealType($type);
        $gang->setCreatedAt(new \DateTime('now'));
        $gang->setUpdatedAt(new \DateTime('now'));

        $this->entityManager->persist($gang);
        $this->entityManager->flush();

        $group = new Item($gang, new GangTransformer());
        return $this->fractal->createData($group)->toArray();
    }

    /**
     * Alter a Group's name
     *
     * @param $id
     * @param $field
     * @param $value
     * @return bool
     * @throws GroupNotFoundException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function alterGroup($id, $field, $value)
    {
        $gang = $this->entityManager->find('AppBundle:Gang', $id);

        if (empty($gang) === true) {
            throw new GroupNotFoundException();
        }

        switch ($field) {
            case 'name':
                $gang->setName($value);
                break;
            default:
                throw new \InvalidArgumentException();
                break;
        }

        $gang->setUpdatedAt(new \DateTime('now'));

        $this->entityManager->persist($gang);
        $this->entityManager->flush();

        return true;
    }

    /**
     * Create a new Person for a Group
     * This new person doesn't have to have an email but, if provided, it will send an invite email to them along with
     * a code they can use to log in.
     *
     * @param $groupId
     * @param $name
     * @param null $email
     * @param null $personId
     * @return array
     * @throws GroupNotFoundException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function createPerson($groupId, $name, $email = null)
    {
        $gang = $this->entityManager->find('AppBundle:Gang', $groupId);

        if (empty($gang) === true) {
            throw new GroupNotFoundException();
        }

        $newMember = null;

        if ($email !== null) {
            if (!preg_match('/^.+\@\S+\.\S+$/', $email)) {
                $email = null;
            } else {
                $memberRepository = $this->entityManager->getRepository('AppBundle:Member');
                $newMember = $memberRepository->findOneBy(['email' => $email]);
            }
        }

        if ($newMember === null) {
            $newMember = new Member();
            $newMember->setName($name);
            $newMember->setEmail($email);
            $newMember->setCreatedAt(new \DateTime('now'));
        }

        $newMember->setToken(bin2hex(random_bytes(16)));
        $newMember->setUpdatedAt(new \DateTime('now'));
        $newMember->setDeletedAt(null);

        $newGangMembership = new GangMembership();
        $newGangMembership->setMember($newMember);
        $newGangMembership->setGang($gang);
        $newGangMembership->setCreatedAt(new \DateTime('now'));
        $newGangMembership->setUpdatedAt(new \DateTime('now'));
        $newGangMembership->setValue(0.00);

        $newMember->addGangMembership($newGangMembership);

        $this->entityManager->persist($newGangMembership);
        $this->entityManager->persist($newMember);
        $this->entityManager->flush();

        if ($email !== null) {
            $this->inviteService->sendInvite($newMember->getId());
        }

        $person = new Item($newGangMembership, new GangMemberTransformer());
        return $this->fractal->createData($person)->toArray();
    }

    /**
     * Removes a Person from a Group.
     * They must be at 0 for that Group in order to be removed.
     * If they don't belong to any other Groups and they are not active the Person will also be removed.
     *
     * @param $groupId
     * @param $personId
     * @return bool
     * @throws GroupMembershipNotFoundException
     * @throws GroupNotFoundException
     * @throws PersonNotFoundException
     * @throws \Exception
     */
    public function removePerson($groupId, $personId)
    {
        $gang = $this->entityManager->find('AppBundle:Gang', $groupId);

        if (empty($gang) === true) {
            throw new GroupNotFoundException();
        }

        $member = $this->entityManager->find('AppBundle:Member', $personId);

        if (empty($member) === true) {
            throw new PersonNotFoundException();
        }

        $gangMembership = null;
        $gangMemberships = $member->getGangMemberships();
        $gangMembershipCount = count($gangMemberships);

        foreach ($gangMemberships as $membership) {
            if ($membership->getGang() === $gang) {
                $gangMembership = $membership;
                break;
            }
        }

        if ($gangMembership === null) {
            throw new GroupMembershipNotFoundException();
        }

        if ((float)$gangMembership->getValue() !== 0.00) {
            throw new \Exception('Can not remove a person from a group if their value is not 0');
        }

        $gangMembership->setDeletedAt(new \DateTime('now'));
        $this->entityManager->persist($gangMembership);

        if ($member->getIsActive() === false && $gangMembershipCount <= 1) {
            $member->setDeletedAt(new \DateTime('now'));
            $this->entityManager->persist($member);
        }

        $this->entityManager->flush();

        return true;
    }

    /**
     * Switch a Group's type from money to simple or from simple to money
     *
     * One use case for this would be a Group that usually buys coffee together at ~$4.50 a pop want to switch to a more
     * accurate 'money' mode. If Damian, Nicci and Sally are on 2, -1 and -1 and supply 4.50 as the factor it will
     * become 9.00, -4.50 and -4.50. If no factor is supplied it will become 2.00, -1.00 and -1.00
     *
     * Another use case is for a Group that uses 'money' mode to track their dinners out and wants to switch it down to
     * 'simple' mode for buying beers. If Damian, Nicci and Sally are on 70.50, -20.25 and -50.25 and supply 9.00 as the
     * factor it will become 8, -2 and -6. If no factor is supplied it will use the value closest to 0 and will result
     * in 3, -1 and -2
     *
     * @param $id
     * @param $type
     * @param float $factor
     * @return array
     * @throws GroupNotFoundException
     * @throws RecalculationException
     */
    public function switchType($id, $type, $factor = 0.00)
    {
        if (Gang::isValidDealType($type) === false) {
            throw new \InvalidArgumentException(sprintf("%s is not a valid group type", $type));
        }
        
        $gangRepository = $this->entityManager->getRepository('AppBundle:Gang');
        $gang = $gangRepository->find($id);

        if (empty($gang) === true) {
            throw new GroupNotFoundException();
        }
        
        if ($gang->getDealType() === $type) {
            throw new \InvalidArgumentException();
        }
        
        $gangMemberships = $gang->getGangMemberships();
        
        // quick check whether we need to adjust Member's values or not
        $needToAdjust = false;
        
        if ($type === Gang::TYPE_SIMPLE || ($type === Gang::TYPE_MONEY && $factor !== 0.00)) {
            foreach ($gangMemberships as $gangMembership) {
                if ($gangMembership->getValue() !== 0.00) {
                    $needToAdjust = true;
                    break;
                }
            }
        }

        if ($needToAdjust === true) {
            // need to calculate the best factor to use when converting to simple type
            if ($type === Gang::TYPE_SIMPLE) {
                if ($factor === 0.00) {
                    $factor = $this->calculateFactorForAutomaticConvertToSimple($gangMemberships);
                } else {
                    $factor = 1 / $factor;
                }
            }

            $zeroSum = 0;
            $rounding = $type === Gang::TYPE_SIMPLE ? 0 : 2;

            foreach ($gangMemberships as $gangMembership) {
                $currentValue = $gangMembership->getValue();
                
                $newValue = round($currentValue * $factor, $rounding);
                $gangMembership->setValue($newValue);
                $gangMembership->setUpdatedAt(new \DateTime('now'));
                $this->entityManager->persist($gangMembership);

                $zeroSum += $newValue;
            }

            if ($zeroSum !== 0.00) {
                $this->resolveNonZeroSum($gangMemberships, $zeroSum);
                //if ($isResolved === false) {
                //    throw new RecalculationException('Something went wrong trying to zero the totals' . $zeroSum);
                //}
            }
        }

        $gang->setDealType($type);
        $gang->setUpdatedAt(new \DateTime('now'));
        $this->entityManager->persist($gang);
        $this->entityManager->flush();
        
        $group = new Item($gang, new GangTransformer());
        return $this->fractal->createData($group)->toArray();
    }

    /**
     * @param GangMembership[] $gangMemberships
     * @param $difference
     * @return float
     */
    private function resolveNonZeroSum($gangMemberships, $difference)
    {
        /** @var GangMembership | null $memberFurthestFromZero */
        $memberFurthestFromZero = null;

        foreach ($gangMemberships as $gangMembership) {
            if ($memberFurthestFromZero === null || abs($gangMembership->getValue()) > abs($memberFurthestFromZero->getValue())) {
                $memberFurthestFromZero = $gangMembership;
            }
        }
        
        $memberFurthestFromZero->setValue($memberFurthestFromZero->getValue() + ($difference * -1));
        $this->entityManager->persist($memberFurthestFromZero);
    }

    /**
     * @param GangMembership[] $gangMemberships
     * @return float
     */
    private function calculateFactorForAutomaticConvertToSimple($gangMemberships)
    {
        /** @var GangMembership | null $memberClosestToZero */
        $memberClosestToZero = null;

        foreach ($gangMemberships as $gangMembership) {
            // we don't count 0 as close to 0 because it gives us no factor to use
            if ($gangMembership->getValue() === 0.00) {
                continue;
            }
            if ($memberClosestToZero === null || abs($gangMembership->getValue()) < abs($memberClosestToZero->getValue())) {
                $memberClosestToZero = $gangMembership;
            }
        }

        $factor = 1 / abs($memberClosestToZero->getValue());
        
        return $factor;
    }

    public function canViewDetails($memberId, $gangId)
    {
        $gangMembershipRepository = $this->entityManager->getRepository('AppBundle:GangMembership');

        $result = $gangMembershipRepository->isInGang($memberId, $gangId);
        return $result;
    }
}
