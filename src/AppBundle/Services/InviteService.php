<?php

namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;
use Monolog\Logger;

class InviteService
{
    protected $entityManager;
    protected $mailer;
    protected $mailFrom;

    public function __construct(
        EntityManager $entityManger,
        \Swift_Mailer $mailer,
        $mailFrom
    ) {
        $this->entityManager = $entityManger;
        $this->mailer = $mailer;
        $this->mailFrom = $mailFrom;
    }

    public function sendCode($personId)
    {
        $person = $this->entityManager->find('AppBundle:Member', $personId);

        $message = \Swift_Message::newInstance()
            ->setSubject(
                sprintf("Your RoundPal code is: %s", $person->getCode())
            )
            ->setFrom($this->mailFrom)
            ->setTo($person->getEmail())
            ->setBody(
                sprintf(
                    "You recently requested access to RoundPal and the code to get in is: %s. If you didn't instigate this then it's possible someone is messing with your email address and you should email me back so I can look into it. Cheers, Damian.",
                    $person->getCode()
                ),
                'text/plain'
            )
        ;

        $success = $this->mailer->send($message);

        return $success;
    }

    public function sendInvite($personId)
    {
        $person = $this->entityManager->find('AppBundle:Member', $personId);

        $message = \Swift_Message::newInstance()
            ->setSubject('Here is your code for RoundPal')
            ->setFrom($this->mailFrom)
            ->setTo($person->getEmail())
            ->setBody("")
        ;

        $success = $this->mailer->send($message);

        return $success;
    }
}
