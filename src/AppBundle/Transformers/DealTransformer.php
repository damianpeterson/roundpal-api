<?php

namespace AppBundle\Transformers;

use AppBundle\Entity\Deal;
use League\Fractal;

class DealTransformer extends Fractal\TransformerAbstract
{
    public function transform(Deal $deal)
    {
        $dealMembers = [];
        
        foreach ($deal->getDealMembers() as $dealMember) {
            $member = $dealMember->getMember();
            $dealMembers[] = [
                'id' => $member->getId(),
                'name' => $member->getName(),
                'value' => number_format($dealMember->getValue(), 2)
            ];
        }
        
        return [
            'id' => $deal->getId(),
            'date' => $deal->getCreatedAt()->format(DATE_ISO8601),
            'group_id' => $deal->getGang()->getId(),
            'people' => $dealMembers
        ];
    }
}
