<?php

namespace AppBundle\Transformers;

use AppBundle\Entity\Member;
use League\Fractal;

class ErrorTransformer extends Fractal\TransformerAbstract
{
    public function transform(array $details = [])
    {
        $code = 400;
        $message = 'Undefined error';
        $fields = [];
        
        if (array_key_exists('code', $details)) {
            $code = $details['code'];
        }

        if (array_key_exists('message', $details)) {
            $message = $details['message'];
        }

        if (array_key_exists('field', $details)) {
            $field = $details['field'];
        }
        
        return [
            'code' => $code,
            'message' => $message,
            'field' => $field
        ];
    }
}
