<?php

namespace AppBundle\Transformers;

use AppBundle\Entity\Gang;
use AppBundle\Entity\GangMembership;
use League\Fractal;

class GangMemberTransformer extends Fractal\TransformerAbstract
{
    public function transform(GangMembership $gangMembership)
    {
        $value = $gangMembership->getValue();
        if ($gangMembership->getGang()->getDealType() === Gang::TYPE_SIMPLE) {
            $value = round($value);
        }

        return [
            'id' => $gangMembership->getMember()->getId(),
            'name' => $gangMembership->getMember()->getName(),
            'email' => $gangMembership->getMember()->getEmail(),
            'value' => $value
        ];
    }
}
