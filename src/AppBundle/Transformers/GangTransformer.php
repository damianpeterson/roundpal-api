<?php

namespace AppBundle\Transformers;

use AppBundle\Entity\Gang;
use League\Fractal;

class GangTransformer extends Fractal\TransformerAbstract
{
    public function transform(Gang $gang)
    {
        return [
            'id' => $gang->getId(),
            'name' => $gang->getName(),
            'type' => $gang->getDealType()
        ];
    }
}
