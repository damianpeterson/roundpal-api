<?php

namespace AppBundle\Transformers;

use AppBundle\Entity\Member;
use League\Fractal;

class MemberTransformer extends Fractal\TransformerAbstract
{
    public function transform(Member $member)
    {
        return [
            'id' => $member->getId(),
            'name' => $member->getName(),
            'email' => $member->getEmail(),
            'isActive' => $member->getIsActive()
        ];
    }
}
