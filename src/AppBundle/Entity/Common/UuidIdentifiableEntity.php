<?php

namespace AppBundle\Entity\Common;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait UuidIdentifiableEntity
{
    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * Gets the value of id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the value of id.
     *
     * @param guid $id
     * @return self
     */
    public function setId($id = null)
    {
        $this->id = $id;

        return $this;
    }
}
