<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Common\UuidIdentifiableEntity;
use AppBundle\Entity\Common\CreatableEntity;
use AppBundle\Entity\Common\UpdatableEntity;
use AppBundle\Entity\Common\DeletableEntity;

/**
 * Member
 *
 * @ORM\Table(
 *      name="members"
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MemberRepository")
 */
class Member
{
    use UuidIdentifiableEntity;
    use CreatableEntity;
    use UpdatableEntity;
    use DeletableEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * Email is only required for Members who want to log in. It's possible to have a Member created by another Member
     * but isn't tied to a real person.
     *
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * The token that is used to authenticate requests to the API for this Member. Can be used across multiple devices.
     * See http://symfony.com/doc/current/cookbook/security/api_key_authentication.html
     *
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=false)
     */
    private $token;

    /**
     * When a Member wants to log in they'll be sent a new, temporary request code which will be used to verify that
     * it's ok to send them their token.
     *
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;
    
    /**
     * Used to ensure that stale codes aren't reused to request a token.
     *
     * @var \DateTime $codeRequestedAt
     *
     * @ORM\Column(name="code_requested_at", type="datetime", nullable=true)
     */
    private $codeRequestedAt;

    /**
     * When this Member last signed in. If they've never signed in they're considered not 'isActive'
     *
     * @var \DateTime $lastSignedInAt
     *
     * @ORM\Column(name="last_signed_in_at", type="datetime", nullable=true)
     */
    private $lastSignedInAt;

    /**
     * A Member can belong to many different Gangs and in each Gang they have a different debt
     *
     * @var ArrayCollection | GangMembership[]
     * @ORM\OneToMany(targetEntity="GangMembership", mappedBy="member")
     */
    private $gangMemberships;

    /**
     * A Member has many Deals
     *
     * @var ArrayCollection | DealMember[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\DealMember", mappedBy="member")
     */
    private $memberDeals;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Member
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return Member
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return \DateTime
     */
    public function getCodeRequestedAt()
    {
        return $this->codeRequestedAt;
    }

    /**
     * @param \DateTime $codeRequestedAt
     */
    public function setCodeRequestedAt($codeRequestedAt)
    {
        $this->codeRequestedAt = $codeRequestedAt;
    }

    /**
     * @return \DateTime
     */
    public function getLastSignedInAt()
    {
        return $this->lastSignedInAt;
    }

    /**
     * @param \DateTime $lastSignedInAt
     */
    public function setLastSignedInAt($lastSignedInAt)
    {
        $this->lastSignedInAt = $lastSignedInAt;
    }

    /**
     * Infers whether a Member is active by whether they've ever signed in before
     * @return bool
     */
    public function getIsActive()
    {
        return isset($this->lastSignedInAt);
    }

    /**
     * @param GangMembership $gangMembership
     * @return Member
     */
    public function addGangMembership(GangMembership $gangMembership)
    {
        $this->gangMemberships[] = $gangMembership;

        return $this;
    }

    /**
     * @param GangMembership $gangMembership
     */
    public function removeGangMembership(GangMembership $gangMembership)
    {
        $this->gangMemberships->removeElement($gangMembership);
    }

    /**
     * @return ArrayCollection | GangMembership[]
     */
    public function getGangMemberships()
    {
        $criteria = Criteria::create()->andWhere(Criteria::expr()->eq('deletedAt', null));
        return $this->gangMemberships->matching($criteria);
    }

    public function getGangMembership(Gang $gang)
    {
        foreach ($this->getGangMemberships() as $gangMembership) {
            if ($gangMembership->getGang() === $gang) {
                return $gangMembership;
            }
        }
        
        return null;
    }

    /**
     * @param DealMember $dealMember
     * @return Member
     */
    public function addMemberDeal(DealMember $dealMember)
    {
        $this->memberDeals[] = $dealMember;

        return $this;
    }

    /**
     * @param DealMember $dealMember
     */
    public function removeMemberDeal(DealMember $dealMember)
    {
        $this->memberDeals->removeElement($dealMember);
    }

    /**
     * @return ArrayCollection | DealMember[]
     */
    public function getMemberDeals()
    {
        return $this->memberDeals;
    }
}
