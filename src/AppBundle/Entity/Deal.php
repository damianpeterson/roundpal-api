<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Common\IdentifiableEntity;
use AppBundle\Entity\Common\CreatableEntity;
use AppBundle\Entity\Common\UpdatableEntity;
use AppBundle\Entity\Common\DeletableEntity;

/**
 * Deal
 *
 * @ORM\Table(name="deals")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DealRepository")
 */
class Deal
{
    use IdentifiableEntity;
    use CreatableEntity;
    use UpdatableEntity;
    use DeletableEntity;

    /**
     * @var Gang $gang
     *
     * @ORM\ManyToOne(targetEntity="Gang", inversedBy="deals")
     * @ORM\JoinColumn(name="gang_id", referencedColumnName="id", nullable=false)
     */
    private $gang;

    /**
     * A Deal is between many Members
     *
     * @var ArrayCollection | DealMember[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\DealMember", mappedBy="deal")
     */
    private $dealMembers;

    /**
     * @param Gang $gang
     * @return Deal
     */
    public function setGang($gang)
    {
        $this->gang = $gang;

        return $this;
    }

    /**
     * @return Gang
     */
    public function getGang()
    {
        return $this->gang;
    }

    /**
     * @param DealMember $dealMember
     * @return Gang
     */
    public function addDealMember(DealMember $dealMember)
    {
        $this->dealMembers[] = $dealMember;

        return $this;
    }

    /**
     * @param DealMember $dealMember
     */
    public function removeDealMember(DealMember $dealMember)
    {
        $this->dealMembers->removeElement($dealMember);
    }

    /**
     * @return ArrayCollection | DealMember[]
     */
    public function getDealMembers()
    {
        return $this->dealMembers;
    }
}
