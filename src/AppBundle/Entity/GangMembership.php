<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Common\IdentifiableEntity;
use AppBundle\Entity\Common\CreatableEntity;
use AppBundle\Entity\Common\UpdatableEntity;
use AppBundle\Entity\Common\DeletableEntity;

/**
 * GangMembership
 *
 * @ORM\Table(
 *      name="gang_memberships"
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GangMembershipRepository")
 */
class GangMembership
{
    use IdentifiableEntity;
    use CreatableEntity;
    use UpdatableEntity;
    use DeletableEntity;

    /**
     * @var Member $member
     *
     * @ORM\ManyToOne(targetEntity="Member", inversedBy="gangMemberships")
     * @ORM\JoinColumn(name="member_id", referencedColumnName="id", nullable=false)
     */
    private $member;

    /**
     * @var Gang $gang
     *
     * @ORM\ManyToOne(targetEntity="Gang", inversedBy="gangMemberships")
     * @ORM\JoinColumn(name="gang_id", referencedColumnName="id", nullable=false)
     */
    private $gang;

    /**
     * How much a Member owes or has owing to them in this Gang
     *
     * @var float
     *
     * @ORM\Column(name="value", type="decimal", scale=2, nullable=false, options={"default":0.00})
     */
    private $value;

    /**
     * @param Member $member
     * @return GangMembership
     */
    public function setMember($member)
    {
        $this->member = $member;

        return $this;
    }

    /**
     * @return Member
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * @param Gang $gang
     * @return GangMembership
     */
    public function setGang($gang)
    {
        $this->gang = $gang;

        return $this;
    }

    /**
     * @return Gang
     */
    public function getGang()
    {
        return $this->gang;
    }

    /**
     * @param float $value
     *
     * @return GangMembership
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }
}
