<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Common\IdentifiableEntity;

/**
 * Deal Member
 *
 * This is the Member's details for a Deal. How much they owe or paid.
 *
 * @ORM\Table(
 *      name="deal_members"
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DealMemberRepository")
 */
class DealMember
{
    use IdentifiableEntity;

    /**
     * @var Member $member
     *
     * @ORM\ManyToOne(targetEntity="Member", inversedBy="memberDeals")
     * @ORM\JoinColumn(name="member_id", referencedColumnName="id", nullable=false)
     */
    private $member;

    /**
     * @var Deal $deal
     *
     * @ORM\ManyToOne(targetEntity="Deal", inversedBy="dealMembers")
     * @ORM\JoinColumn(name="deal_id", referencedColumnName="id", nullable=false)
     */
    private $deal;

    /**
     * How much a Member paid or owes for this Deal
     *
     * @var float
     *
     * @ORM\Column(name="value", type="decimal", scale=2, nullable=false, options={"default":0.00})
     */
    private $value;

    /**
     * @return Member
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * @param Member $member
     *
     * @return DealMember
     */
    public function setMember($member)
    {
        $this->member = $member;
        
        return $this;
    }

    /**
     * @return Deal
     */
    public function getDeal()
    {
        return $this->deal;
    }

    /**
     * @param Deal $deal
     *
     * @return DealMember
     */
    public function setDeal($deal)
    {
        $this->deal = $deal;

        return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     *
     * @return DealMember
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}
