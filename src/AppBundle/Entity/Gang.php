<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Common\UuidIdentifiableEntity;
use AppBundle\Entity\Common\CreatableEntity;
use AppBundle\Entity\Common\UpdatableEntity;
use AppBundle\Entity\Common\DeletableEntity;

/**
 * Gang
 * Named so because 'group' is a reserved word. It's kind of awesome though.
 * It was going to be People, Groups and Transactions but now it can be Members, Gangs and Deals.
 *
 * @ORM\Table(name="gangs")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GangRepository")
 */
class Gang
{
    const TYPE_SIMPLE = 'simple';
    const TYPE_MONEY = 'money';
    
    use UuidIdentifiableEntity;
    use CreatableEntity;
    use UpdatableEntity;
    use DeletableEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * What type of Deals this Gang does.
     * 'simple' is integers only. unders and overs
     * 'money' tracks decimal places and allows for custom splitting of bills
     *
     * @var string
     *
     * @ORM\Column(name="deal_type", type="string", length=255, nullable=false)
     */
    private $dealType;

    /**
     * Gangs can have many Members, each of whom might be in debt or have money owed to them
     *
     * @var ArrayCollection | GangMembership[]
     * @ORM\OneToMany(targetEntity="GangMembership", mappedBy="gang")
     */
    private $gangMemberships;

    /**
     * Gangs can have many Deals
     *
     * @var ArrayCollection | Deal[]
     * @ORM\OneToMany(targetEntity="Deal", mappedBy="gang")
     */
    private $deals;

    /**
     * @param string $name
     *
     * @return Gang
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $dealType
     *
     * @return Gang
     */
    public function setDealType($dealType)
    {
        $this->dealType = $dealType;

        return $this;
    }

    /**
     * @return string
     */
    public function getDealType()
    {
        return $this->dealType;
    }
    
    public static function isValidDealType($type)
    {
        return ($type === self::TYPE_MONEY || $type === self::TYPE_SIMPLE);
    }

    /**
     * @param GangMembership $gangMembership
     * @return Gang
     */
    public function addGangMembership(GangMembership $gangMembership)
    {
        $this->gangMemberships[] = $gangMembership;

        return $this;
    }

    /**
     * @param GangMembership $gangMembership
     */
    public function removeGangMembership(GangMembership $gangMembership)
    {
        $this->gangMemberships->removeElement($gangMembership);
    }

    /**
     * @return ArrayCollection | GangMembership[]
     */
    public function getGangMemberships()
    {
        $criteria = Criteria::create()->andWhere(Criteria::expr()->eq('deletedAt', null));
        return $this->gangMemberships->matching($criteria);
    }

    /**
     * @param Deal $deal
     * @return Gang
     */
    public function addDeal(Deal $deal)
    {
        $this->deals[] = $deal;

        return $this;
    }

    /**
     * @param Deal $deal
     */
    public function removeDeal(Deal $deal)
    {
        $this->deals->removeElement($deal);
    }

    /**
     * @return ArrayCollection | Deal[]
     */
    public function getDeals()
    {
        $criteria = Criteria::create()->andWhere(Criteria::expr()->eq('deletedAt', null));
        return $this->deals->matching($criteria);
    }
}
