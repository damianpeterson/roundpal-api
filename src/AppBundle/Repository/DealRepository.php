<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Gang;
use Doctrine\ORM\EntityRepository;

/**
 * DealRepository
 */
class DealRepository extends EntityRepository
{
    public function getDealsForGang(Gang $gang, $offset = 0, $limit = 100)
    {
        $queryBuilder = $this->createQueryBuilder('deals');
        $queryBuilder
            ->where('deals.gang = :gang')
            ->andWhere('deals.deletedAt IS NULL')
            ->orderBy('deals.createdAt', 'ASC')
            ->setParameter('gang', $gang)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;

        return $queryBuilder->getQuery()->getResult();
    }
}
