<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * GangMembershipRepository
 */
class GangMembershipRepository extends EntityRepository
{
    public function getGangMembershipsForGang($gang)
    {
        $queryBuilder = $this->createQueryBuilder('gang_memberships');
        $queryBuilder
            ->join('gang_memberships.member', 'members')
            ->where('gang_memberships.gang = :gang')
            ->andWhere('gang_memberships.deletedAt IS NULL')
            ->andWhere('members.deletedAt IS NULL')
            ->setParameter('gang', $gang);
            
        return $queryBuilder->getQuery()->getResult();
    }

    public function isInGang($memberId, $gangId)
    {
        $queryBuilder = $this->createQueryBuilder('gang_memberships');
        $queryBuilder
            ->select('COUNT(gang_memberships)')
            ->where('gang_memberships.gang = :gangId')
            ->andWhere('gang_memberships.member = :memberId')
            ->andWhere('gang_memberships.deletedAt IS NULL')
            ->setParameter('gangId', $gangId)
            ->setParameter('memberId', $memberId)
        ;

        $result = $queryBuilder->getQuery()->getSingleScalarResult();

        return (Boolean)$result;
    }

    public function bothInSameGang($memberId, $targetMemberId)
    {
        $rsm = new ResultSetMapping();
        $query = $this->getEntityManager()->createNativeQuery('
            SELECT gm1.id
            FROM gang_memberships gm1
            WHERE
                gm1.member_id = ?
                AND gm1.deleted_at IS NULL
                AND gm1.gang_id IN (
                    SELECT gm2.gang_id
                    FROM gang_memberships gm2
                    WHERE gm2.member_id = ?
                    AND gm2.deleted_at IS NULL
                )
        ', $rsm);

        $query->setParameter(1, $memberId);
        $query->setParameter(2, $targetMemberId);

        $result = $query->getScalarResult();
        return count($result) > 0;
    }
}
