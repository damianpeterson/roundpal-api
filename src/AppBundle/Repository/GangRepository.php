<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Member;
use Doctrine\ORM\EntityRepository;

/**
 * GangRepository
 */
class GangRepository extends EntityRepository
{
    public function getGangsForMember(Member $member)
    {
        $queryBuilder = $this->createQueryBuilder('gangs');
        $queryBuilder
            ->join('gangs.gangMemberships', 'gang_memberships')
            ->where('gang_memberships.member = :member')
            ->andWhere('gangs.deletedAt IS NULL')
            ->orderBy('gang_memberships.createdAt', 'ASC')
            ->setParameter('member', $member)
        ;

        return $queryBuilder->getQuery()->getResult();
    }
}
