![RoundPal](https://api.roundpal.com/roundpal.png)

# RoundPal API

RoundPal solves the problem of keeping track of money when you want to share. Ideal for your morning coffee group, your
drinking buddies or when travelling with friends. The best bit is that it doesn't need the same people every time to 
work; people can drop in and out of your group and RoundPal will keep things fair.

## Structure

This project is the API part of the application. It's open source. In the `/documentation` directory is an interactive 
Swagger page which allows you to hit all the endpoints. You can log in there, get an API key and use that to make the 
rest of the calls.

The front end is also open source and is [available on GitLab](https://gitlab.com/damianpeterson/roundpal-web).

## Tests

All tests are automatically run on git pre-commit using `bin/grumphp run` but you can run them individually as per below:

- PHP CodeSniffer for PSR2 standards `bin/phpcs src/AppBundle` (if you want it to fix on the fly: 
`bin/phpcbf src/AppBundle`)
- PHPSpec to ensure the classes follow the specification `bin/phpspec run`
- Codeception to perform acceptance tests on the API endpoints `bin/codecept run api`
- Security checker to make sure your packages are up to date: `bin/security-checker security:check`

## A bit about the architecture

This project was built using TDD and showcases service-layer architecture. The database entities have been deliberately 
abstracted so you can see a clear line between what the consumer understands of the app and what the service layer 
understands of the database. You'll notice that the actual database entities have a slightly seedy aspect to them.

i.e. a `Person` maps beneath the service layer to a `Member`, a `Group` maps to a `Gang` and a `Transaction` to `Deal`

If you're editing a controller or command or hitting an API endpoint you should never see a reference to the seedy side 
of things. You'll never see a reference to a `GangMember`. That should all be below the service layer.

## Running it yourself

If you'd like to run and test the API yourself, pull it from GitHub and create two databases; one as per whatever you 
define in the `parameters.yml` file and one called `roundpal_test` as per the `config_test.yml` file. 

Run `composer install` from the root directory and then run `bin/console doctrine:migrations:migrate` to create the 
primary database tables. The `roundpal_test` database will be automatically rewritten during testing.

At this stage you should be able to run `bin/grumphp run` to complete all the tests.

In order for the documentation (see top of page) to work you'll need to have the word, `localhost` or `dev` in your URL.
I tend to go with `dev.api.roundpal.com`.
