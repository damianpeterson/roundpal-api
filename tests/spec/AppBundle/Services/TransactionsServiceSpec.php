<?php

namespace spec\AppBundle\Services;

use AppBundle\Entity\Deal;
use AppBundle\Entity\DealMember;
use AppBundle\Entity\Gang;
use AppBundle\Entity\GangMembership;
use AppBundle\Entity\Member;
use AppBundle\Exceptions\GroupNotFoundException;
use AppBundle\Exceptions\InvalidTransactionException;
use AppBundle\Exceptions\PersonNotFoundException;
use AppBundle\Repository\DealRepository;
use AppBundle\Services\TransactionsService;
use Doctrine\ORM\EntityManager;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class TransactionsServiceSpec
 * @package spec\AppBundle\Services
 *
 * @mixin TransactionsService
 */
class TransactionsServiceSpec extends ObjectBehavior
{
    function let(EntityManager $entityManager)
    {
        $this->beConstructedWith($entityManager);
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType('AppBundle\Services\TransactionsService');
    }

    function it_gets_transactions_for_a_group(
        EntityManager $entityManager,
        DealRepository $dealRepository,
        Gang $gang,
        Deal $mondayDeal,
        Deal $tuesdayDeal,
        DealMember $mondayBob,
        DealMember $mondaySam,
        DealMember $mondayEve,
        DealMember $tuesdayBob,
        DealMember $tuesdayEve,
        Member $bob,
        Member $sam,
        Member $eve
    )
    {
        $gang->getId()->willReturn('group111');
        
        $bob->getId()->willReturn('id_bob');
        $bob->getName()->willReturn('Bob');
        $sam->getId()->willReturn('id_sam');
        $sam->getName()->willReturn('Sam');
        $eve->getId()->willReturn('id_eve');
        $eve->getName()->willReturn('Eve');
        
        $mondayBob->getMember()->willReturn($bob);
        $mondayBob->getValue()->willReturn(2);
        $mondaySam->getMember()->willReturn($sam);
        $mondaySam->getValue()->willReturn(-1);
        $mondayEve->getMember()->willReturn($eve);
        $mondayEve->getValue()->willReturn(-1);
        $tuesdayBob->getMember()->willReturn($bob);
        $tuesdayBob->getValue()->willReturn(-1);
        $tuesdayEve->getMember()->willReturn($eve);
        $tuesdayEve->getValue()->willReturn(1);

        $mondayDeal->getId()->willReturn('id_monday_deal');
        $mondayDeal->getCreatedAt()->willReturn(new \DateTime('2016-01-01 00:11:22'));
        $mondayDeal->getDealMembers()->willReturn([$mondayBob, $mondaySam, $mondayEve]);
        $mondayDeal->getGang()->willReturn($gang);
        $tuesdayDeal->getId()->willReturn('id_tuesday_deal');
        $tuesdayDeal->getCreatedAt()->willReturn(new \DateTime('2016-01-02 11:22:33'));
        $tuesdayDeal->getDealMembers()->willReturn([$tuesdayBob, $tuesdayEve]);
        $tuesdayDeal->getGang()->willReturn($gang);
        
        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $entityManager->getRepository('AppBundle:Deal')->willReturn($dealRepository);
        $dealRepository->getDealsForGang($gang, null, null)->willReturn([$mondayDeal, $tuesdayDeal]);
        
        $transactions = $this->getTransactionsForGroup('group111', null, null);
        $transactions->shouldEqual([
            'data' => [
                0 => [
                    'id' => 'id_monday_deal',
                    'date' => '2016-01-01T00:11:22+0000',
                    'group_id' => 'group111',
                    'people' => [
                        0 => [
                            'id' => 'id_bob',
                            'name' => 'Bob',
                            'value' => '2.00'
                        ],
                        1 => [
                            'id' => 'id_sam',
                            'name' => 'Sam',
                            'value' => '-1.00'
                        ],
                        2 => [
                            'id' => 'id_eve',
                            'name' => 'Eve',
                            'value' => '-1.00'
                        ]
                    ]
                ],
                1 => [
                    'id' => 'id_tuesday_deal',
                    'date' => '2016-01-02T11:22:33+0000',
                    'group_id' => 'group111',
                    'people' => [
                        0 => [
                            'id' => 'id_bob',
                            'name' => 'Bob',
                            'value' => '-1.00'
                        ],
                        1 => [
                            'id' => 'id_eve',
                            'name' => 'Eve',
                            'value' => '1.00'
                        ]
                    ]
                ]
            ]
        ]);
    }

    public function it_throws_exception_for_invalid_group_id_when_getting_transactions(EntityManager $entityManager)
    {
        $entityManager->find('AppBundle:Gang', 'groupXXX')->willReturn([]);
        $this->shouldThrow(new GroupNotFoundException())->during('getTransactionsForGroup', ['id' => 'groupXXX']);
    }

    function it_creates_a_new_transaction(
        EntityManager $entityManager,
        Gang $gang,
        Deal $mondayDeal,
        DealMember $mondayBob,
        DealMember $mondaySam,
        DealMember $mondayEve,
        Member $bob,
        Member $sam,
        Member $eve,
        GangMembership $bobMembership,
        GangMembership $samMembership,
        GangMembership $eveMembership
    )
    {
        $gang->getId()->willReturn('group111');
        $gang->getDealType()->willReturn(Gang::TYPE_SIMPLE);

        $bobMembership->getMember()->willReturn($bob);
        $bobMembership->getValue()->willReturn(10);
        $samMembership->getMember()->willReturn($sam);
        $samMembership->getValue()->willReturn(2);
        $eveMembership->getMember()->willReturn($eve);
        $eveMembership->getValue()->willReturn(-12);

        $bob->getId()->willReturn('id_bob');
        $bob->getName()->willReturn('Bob');
        $bob->getGangMembership($gang)->willReturn($bobMembership);
        $sam->getId()->willReturn('id_sam');
        $sam->getName()->willReturn('Sam');
        $sam->getGangMembership($gang)->willReturn($samMembership);
        $eve->getId()->willReturn('id_eve');
        $eve->getName()->willReturn('Eve');
        $eve->getGangMembership($gang)->willReturn($eveMembership);

        $mondayBob->getMember()->willReturn($bob);
        $mondayBob->getValue()->willReturn(2);
        $mondaySam->getMember()->willReturn($sam);
        $mondaySam->getValue()->willReturn(-1);
        $mondayEve->getMember()->willReturn($eve);
        $mondayEve->getValue()->willReturn(-1);

        $mondayDeal->getId()->willReturn('id_monday_deal');
        $mondayDeal->getCreatedAt()->willReturn(new \DateTime('2016-01-01 00:11:22'));
        $mondayDeal->getDealMembers()->willReturn([$mondayBob, $mondaySam, $mondayEve]);
        $mondayDeal->getGang()->willReturn($gang);

        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $entityManager->find('AppBundle:Member', 'id_bob')->willReturn($bob);
        $entityManager->find('AppBundle:Member', 'id_sam')->willReturn($sam);
        $entityManager->find('AppBundle:Member', 'id_eve')->willReturn($eve);

        $bobMembership->setValue(12)->shouldBeCalled();
        $samMembership->setValue(1)->shouldBeCalled();
        $eveMembership->setValue(-13)->shouldBeCalled();

        $entityManager->persist(Argument::any())->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $transaction = $this->createTransaction('group111', [
            [
                'id' => 'id_bob',
                'value' => '2.00'
            ],
            [
                'id' => 'id_sam',
                'value' => '-1.00'
            ],
            [
                'id' => 'id_eve',
                'value' => '-1.00'
            ]
        ]);

        $transaction->shouldHaveKey('data');
        $transaction['data']->shouldHaveKey('id'); // unable to predict
        $transaction['data']->shouldHaveKey('date'); // unable to predict
        $transaction['data']->shouldHaveKeyWithValue('group_id', 'group111');
        $transaction['data']->shouldHaveKey('people');
        $transaction['data']['people']->shouldEqual([
            0 => [
                'id' => 'id_bob',
                'name' => 'Bob',
                'value' => '2.00'
            ],
            1 => [
                'id' => 'id_sam',
                'name' => 'Sam',
                'value' => '-1.00'
            ],
            2 => [
                'id' => 'id_eve',
                'name' => 'Eve',
                'value' => '-1.00'
            ]
        ]);
    }

    function it_creates_a_new_money_transaction(
        EntityManager $entityManager,
        Gang $gang,
        Deal $mondayDeal,
        DealMember $mondayBob,
        DealMember $mondaySam,
        DealMember $mondayEve,
        Member $bob,
        Member $sam,
        Member $eve,
        GangMembership $bobMembership,
        GangMembership $samMembership,
        GangMembership $eveMembership
    )
    {
        $gang->getId()->willReturn('group111');
        $gang->getDealType()->willReturn(Gang::TYPE_SIMPLE);

        $bobMembership->getMember()->willReturn($bob);
        $bobMembership->getValue()->willReturn(10.00);
        $samMembership->getMember()->willReturn($sam);
        $samMembership->getValue()->willReturn(2.00);
        $eveMembership->getMember()->willReturn($eve);
        $eveMembership->getValue()->willReturn(-12.00);

        $bob->getId()->willReturn('id_bob');
        $bob->getName()->willReturn('Bob');
        $bob->getGangMembership($gang)->willReturn($bobMembership);
        $sam->getId()->willReturn('id_sam');
        $sam->getName()->willReturn('Sam');
        $sam->getGangMembership($gang)->willReturn($samMembership);
        $eve->getId()->willReturn('id_eve');
        $eve->getName()->willReturn('Eve');
        $eve->getGangMembership($gang)->willReturn($eveMembership);

        $mondayBob->getMember()->willReturn($bob);
        $mondayBob->getValue()->willReturn(2.00);
        $mondaySam->getMember()->willReturn($sam);
        $mondaySam->getValue()->willReturn(-1.00);
        $mondayEve->getMember()->willReturn($eve);
        $mondayEve->getValue()->willReturn(-1.00);
        
        $mondayDeal->getId()->willReturn('id_monday_deal');
        $mondayDeal->getCreatedAt()->willReturn(new \DateTime('2016-01-01 00:11:22'));
        $mondayDeal->getDealMembers()->willReturn([$mondayBob, $mondaySam, $mondayEve]);
        $mondayDeal->getGang()->willReturn($gang);
        
        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $entityManager->find('AppBundle:Member', 'id_bob')->willReturn($bob);
        $entityManager->find('AppBundle:Member', 'id_sam')->willReturn($sam);
        $entityManager->find('AppBundle:Member', 'id_eve')->willReturn($eve);
        
        $bobMembership->setValue(12.00)->shouldBeCalled();
        $samMembership->setValue(1.00)->shouldBeCalled();
        $eveMembership->setValue(-13.00)->shouldBeCalled();
        
        $entityManager->persist(Argument::any())->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $transaction = $this->createTransaction('group111', [
            [
                'id' => 'id_bob',
                'value' => '2.00'
            ],
            [
                'id' => 'id_sam',
                'value' => '-1.00'
            ],
            [
                'id' => 'id_eve',
                'value' => '-1.00'
            ]
        ]);

        $transaction->shouldHaveKey('data');
        $transaction['data']->shouldHaveKey('id'); // unable to predict
        $transaction['data']->shouldHaveKey('date'); // unable to predict
        $transaction['data']->shouldHaveKeyWithValue('group_id', 'group111');
        $transaction['data']->shouldHaveKey('people');
        $transaction['data']['people']->shouldEqual([
            0 => [
                'id' => 'id_bob',
                'name' => 'Bob',
                'value' => '2.00'
            ],
            1 => [
                'id' => 'id_sam',
                'name' => 'Sam',
                'value' => '-1.00'
            ],
            2 => [
                'id' => 'id_eve',
                'name' => 'Eve',
                'value' => '-1.00'
            ]
        ]);
    }

    public function it_throws_exception_for_invalid_group_id_when_creating_transaction(EntityManager $entityManager)
    {
        $entityManager->find('AppBundle:Gang', 'groupXXX')->willReturn([]);
        $this->shouldThrow(new GroupNotFoundException())->during('createTransaction', ['groupXXX', []]);
    }

    public function it_throws_exception_for_invalid_person_id_when_creating_transaction(
        EntityManager $entityManager,
        Gang $gang,
        Member $bob
    )
    {
        $gang->getId()->willReturn('group111');
        $gang->getDealType()->willReturn(Gang::TYPE_SIMPLE);
        $bob->getId()->willReturn('id_bob');
        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $entityManager->find('AppBundle:Member', 'id_bob')->willReturn($bob);
        $entityManager->find('AppBundle:Member', 'id_bad')->willReturn(null);
        $this->shouldThrow(new PersonNotFoundException())->during('createTransaction', [
            'group111', 
            [
                ['id' => 'id_bad', 'value' => '1.00'],
                ['id' => 'id_bob', 'value' => '-1.00']
            ]
        ]);
    }

    public function it_throws_exception_for_missing_person_id_when_creating_transaction(
        EntityManager $entityManager,
        Gang $gang
    )
    {
        $gang->getId()->willReturn('group111');
        $gang->getDealType()->willReturn(Gang::TYPE_SIMPLE);
        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $this->shouldThrow(new InvalidTransactionException('Missing id or value or the value is not a number'))->during('createTransaction', [
            'group111',
            [
                ['value' => '1.00'],
                ['id' => 'id_sam', 'value' => '-1.00']
            ]
        ]);
    }

    public function it_throws_exception_for_missing_value_when_creating_transaction(
        EntityManager $entityManager,
        Gang $gang
    )
    {
        $gang->getId()->willReturn('group111');
        $gang->getDealType()->willReturn(Gang::TYPE_SIMPLE);
        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $this->shouldThrow(new InvalidTransactionException('Missing id or value or the value is not a number'))->during('createTransaction', [
            'group111',
            [
                ['id' => 'id_bob'],
                ['id' => 'id_sam', 'value' => '-1.00']
            ]
        ]);
    }

    public function it_throws_exception_for_non_number_value_when_creating_transaction(
        EntityManager $entityManager,
        Gang $gang
    )
    {
        $gang->getId()->willReturn('group111');
        $gang->getDealType()->willReturn(Gang::TYPE_SIMPLE);
        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $this->shouldThrow(new InvalidTransactionException('Missing id or value or the value is not a number'))->during('createTransaction', [
            'group111',
            [
                ['id' => 'id_bob', 'value' => 'nonnumber'],
                ['id' => 'id_sam', 'value' => '-1.00']
            ]
        ]);
    }

    public function it_throws_exception_for_non_zero_sum_when_creating_transaction(
        EntityManager $entityManager,
        Gang $gang,
        Member $bob,
        Member $sam,
        GangMembership $bobGangMembership,
        GangMembership $samGangMembership
    )
    {
        $gang->getId()->willReturn('group111');
        $gang->getDealType()->willReturn(Gang::TYPE_SIMPLE);

        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $entityManager->find('AppBundle:Member', 'id_bob')->willReturn($bob);
        $entityManager->find('AppBundle:Member', 'id_sam')->willReturn($sam);

        $bob->getGangMembership($gang)->willReturn($bobGangMembership);
        $sam->getGangMembership($gang)->willReturn($samGangMembership);

        $entityManager->persist(Argument::any())->shouldBeCalled();

        $this->shouldThrow(new InvalidTransactionException('The values supplied do not add up to zero'))->during('createTransaction', [
            'group111',
            [
                ['id' => 'id_bob', 'value' => '2.00'],
                ['id' => 'id_sam', 'value' => '-1.00']
            ]
        ]);
    }

    /**
     * You shouldn't be able to have the same person more than once in a transaction
     */
    function it_requires_each_person_be_unique(
        EntityManager $entityManager,
        Gang $gang,
        Member $bob,
        Member $sam,
        GangMembership $bobGangMembership,
        GangMembership $samGangMembership
    )
    {
        $gang->getId()->willReturn('group111');
        $gang->getDealType()->willReturn(Gang::TYPE_SIMPLE);

        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $entityManager->find('AppBundle:Member', 'id_bob')->willReturn($bob);
        $entityManager->find('AppBundle:Member', 'id_sam')->willReturn($sam);

        $bob->getGangMembership($gang)->willReturn($bobGangMembership);
        $sam->getGangMembership($gang)->willReturn($samGangMembership);

        $entityManager->persist(Argument::any())->shouldBeCalled();

        $this->shouldThrow(new InvalidTransactionException('Each person must be unique'))->during('createTransaction', [
            'group111',
            [
                ['id' => 'id_bob', 'value' => '2.00'],
                ['id' => 'id_bob', 'value' => '-1.00'],
                ['id' => 'id_sam', 'value' => '-1.00']
            ]
        ]);
    }

    /**
     * On Monday Damian, Mikey and Rob go for coffee where Damian buys for Mikey and Rob leaving them +2, -1, -1
     * On Tuesday Damian and Rob go for coffee and Rob buys leaving them +1 and 0
     */
    function it_adjusts_peoples_group_value_after_a_transaction(
        EntityManager $entityManager,
        Gang $gang,
        Member $bob,
        Member $sam,
        GangMembership $bobGangMembership,
        GangMembership $samGangMembership
    )
    {
        $gang->getId()->willReturn('group111');
        $gang->getDealType()->willReturn(Gang::TYPE_SIMPLE);
        
        $bobGangMembership->getValue()->willReturn(5);
        $samGangMembership->getValue()->willReturn(-5);

        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $entityManager->find('AppBundle:Member', 'id_bob')->willReturn($bob);
        $entityManager->find('AppBundle:Member', 'id_sam')->willReturn($sam);

        $bob->getGangMembership($gang)->willReturn($bobGangMembership);
        $sam->getGangMembership($gang)->willReturn($samGangMembership);
        
        $bobGangMembership->setValue(4)->shouldBeCalled();
        $samGangMembership->setValue(-4)->shouldBeCalled();

        $entityManager->persist($bobGangMembership)->shouldBeCalled();
        $entityManager->persist($samGangMembership)->shouldBeCalled();
        $entityManager->persist(Argument::type('AppBundle\Entity\DealMember'))->shouldBeCalledTimes(2);
        $entityManager->persist(Argument::type('AppBundle\Entity\Deal'))->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();
        
        $bob->getId()->willReturn('id_bob');
        $bob->getName()->willReturn('Bob');
        $sam->getId()->willReturn('id_sam');
        $sam->getName()->willReturn('Sam');

        $transaction = $this->createTransaction(
            'group111',
            [
                ['id' => 'id_bob', 'value' => '-1.00'],
                ['id' => 'id_sam', 'value' => '1.00']
            ]
        );
        
        $transaction->shouldHaveKey('data');
        $transaction['data']->shouldHaveKey('id');
        $transaction['data']->shouldHaveKey('date');
        $transaction['data']->shouldHaveKey('people');
        $transaction['data']['people']->shouldEqual([
            0 => [
                'id' => 'id_bob',
                'name' => 'Bob',
                'value' => '-1.00'
            ],
            1 => [
                'id' => 'id_sam',
                'name' => 'Sam',
                'value' => '1.00'
            ]
        ]);
    }

    /*
    function it_reverses_a_transaction()
    {

    }
    */
}
