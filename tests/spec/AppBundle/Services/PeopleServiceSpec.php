<?php

namespace spec\AppBundle\Services;

use AppBundle\Entity\Gang;
use AppBundle\Entity\Member;
use AppBundle\Exceptions\ExpiredCodeException;
use AppBundle\Exceptions\PersonNotFoundException;
use AppBundle\Repository\GangMembershipRepository;
use AppBundle\Repository\GangRepository;
use AppBundle\Repository\MemberRepository;
use AppBundle\Services\PeopleService;
use AppBundle\Services\InviteService;
use Monolog\Logger;
use Doctrine\ORM\EntityManager;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class PeopleServiceSpec
 * @package spec\AppBundle\Services
 * 
 * @mixin PeopleService
 */
class PeopleServiceSpec extends ObjectBehavior
{
    function let(EntityManager $entityManager, InviteService $inviteService, Logger $logger)
    {
        $this->beConstructedWith($entityManager, $inviteService, 86400, $logger);
    }

    function letGo()
    {
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType('AppBundle\Services\PeopleService');
    }

    function it_gets_a_person_by_their_api_key(
        EntityManager $entityManager,
        MemberRepository $memberRepository,
        Member $member
    )
    {
        $member->getEmail()->willReturn('person111@test.com');
        $member->getId()->willReturn('person111');
        $member->getName()->willReturn('person one');
        $member->getIsActive()->willReturn(true);
        $entityManager->getRepository('AppBundle:Member')->willReturn($memberRepository);
        $memberRepository->findOneBy(['token' => 'person111_apikey'])->willReturn($member);

        $person = $this->getPersonByApiKey('person111_apikey');
        $person->shouldBeArray();
        $person->shouldHaveCount(1);
        $person->shouldEqual([
            'data' => [
                'id' => 'person111',
                'name' => 'person one',
                'email' =>'person111@test.com',
                'isActive' => true,
            ]
        ]);
    }

    function it_returns_a_person_not_found_exception_for_an_invalid_api_key(
        EntityManager $entityManager,
        MemberRepository $memberRepository
    )
    {
        $entityManager->getRepository('AppBundle:Member')->willReturn($memberRepository);
        $memberRepository->findOneBy(['token' => 'person111_apikey'])->willReturn(null);

        $this->shouldThrow(new PersonNotFoundException())->during('getPersonByApiKey', ['person111_apikey']);
    }

    function it_gets_a_person_by_their_email_address(
        EntityManager $entityManager,
        MemberRepository $memberRepository,
        Member $member
    )
    {
        $member->getEmail()->willReturn('person111@test.com');
        $member->getId()->willReturn('person111');
        $member->getName()->willReturn('person one');
        $member->getIsActive()->willReturn(true);
        $entityManager->getRepository('AppBundle:Member')->willReturn($memberRepository);
        $memberRepository->findOneBy(['email' => 'person111@test.com'])->willReturn($member);

        $person = $this->getPersonByEmail('person111@test.com');
        $person->shouldBeArray();
        $person->shouldHaveCount(1);
        $person->shouldEqual([
            'data' => [
                'id' => 'person111',
                'name' => 'person one',
                'email' =>'person111@test.com',
                'isActive' => true,
            ]
        ]);
    }

    function it_returns_a_person_not_found_exception_for_an_invalid_email_address(
        EntityManager $entityManager,
        MemberRepository $memberRepository
    )
    {
        $entityManager->getRepository('AppBundle:Member')->willReturn($memberRepository);
        $memberRepository->findOneBy(['email' => 'person111@test.com'])->willReturn(null);

        $this->shouldThrow(new PersonNotFoundException())->during('getPersonByEmail', ['person111@test.com']);
    }

    function it_creates_a_new_code_for_a_person(
        EntityManager $entityManager,
        Member $member
    )
    {
        $member->getEmail()->willReturn('person111@test.com');
        $member->getId()->willReturn('person111');
        $member->getName()->willReturn('person one');
        $member->setCode(Argument::that(function ($val) {return strlen($val) === 4;}))->shouldBeCalled();
        $member->setUpdatedAt(Argument::type('DateTime'))->shouldBeCalled();
        $member->setCodeRequestedAt(Argument::type('DateTime'))->shouldBeCalled();

        $entityManager->persist($member)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();
        $entityManager->find('AppBundle:Member', 'person111')->willReturn($member);

        $code = $this->createRequestCode('person111');
        $code->shouldBeString();
    }

    function it_gets_a_token_for_a_person_when_provided_a_matching_code(
        EntityManager $entityManager,
        Member $member
    )
    {
        $member->getEmail()->willReturn('person111@test.com');
        $member->getId()->willReturn('person111');
        $member->getName()->willReturn('person one');
        $member->getCode()->willReturn('1234');
        $member->getCodeRequestedAt()->willReturn(new \DateTime('-2 hour'));
        $member->getToken()->shouldBeCalled();
        $member->setCode(null)->shouldBeCalled();
        $member->setUpdatedAt(Argument::type('DateTime'))->shouldBeCalled();
        $member->setLastSignedInAt(Argument::type('DateTime'))->shouldBeCalled();
        $member->setCodeRequestedAt(null)->shouldBeCalled();

        $entityManager->persist($member)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();
        $entityManager->find('AppBundle:Member', 'person111')->willReturn($member);

        $this->getTokenForCode('person111', '1234', true);
    }

    function it_returns_not_found_when_getting_a_token_for_a_person_with_wrong_code(
        EntityManager $entityManager
    )
    {
        $entityManager->find('AppBundle:Member', 'person111')->willReturn(null);

        $this->shouldThrow(new PersonNotFoundException())->during('getTokenForCode', ['person111', '1234', true]);
    }

    function it_returns_forbidden_when_getting_a_token_for_a_person_with_no_code(
        EntityManager $entityManager,
        Member $member
    )
    {
        $member->getCode()->willReturn(null);
        $entityManager->find('AppBundle:Member', 'person111')->willReturn($member);

        $this->shouldThrow(new ExpiredCodeException())->during('getTokenForCode', ['person111', '1234', true]);
    }

    function it_returns_forbidden_when_getting_a_token_for_a_person_with_expired_code(
        EntityManager $entityManager,
        Member $member
    )
    {
        $member->getCode()->willReturn('1234');
        $member->getCodeRequestedAt()->willReturn(new \DateTime('-2 day'));
        $entityManager->find('AppBundle:Member', 'person111')->willReturn($member);

        $this->shouldThrow(new ExpiredCodeException())->during('getTokenForCode', ['person111', '1234', true]);

        // also for a null date
        $member->getCodeRequestedAt()->willReturn(null);
        $this->shouldThrow(new ExpiredCodeException())->during('getTokenForCode', ['person111', '1234', true]);
    }

    function it_resets_a_token_for_a_person(
        EntityManager $entityManager,
        Member $member
    )
    {
        $member->getEmail()->willReturn('person111@test.com');
        $member->getId()->willReturn('person111');
        $member->getName()->willReturn('person one');
        $member->setCode(null)->shouldBeCalled();
        $member->setToken(Argument::that(function ($val) {return strlen($val) === 32;}))->shouldBeCalled();
        $member->setUpdatedAt(Argument::type('DateTime'))->shouldBeCalled();
        $member->setCodeRequestedAt(null)->shouldBeCalled();

        $entityManager->persist($member)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();
        $entityManager->find('AppBundle:Member', 'person111')->willReturn($member);

        $this->resetToken('person111');
    }

    function it_gets_all_people(
        EntityManager $entityManager,
        MemberRepository $memberRepository,
        Member $member1,
        Member $member2
    )
    {
        $member1->getId()->willReturn('person111');
        $member1->getName()->willReturn('person one');
        $member1->getEmail()->willReturn('person111@test.com');
        $member1->getIsActive()->willReturn(true);
        $member2->getId()->willReturn('person222');
        $member2->getName()->willReturn('person two');
        $member2->getEmail()->willReturn('person222@test.com');
        $member2->getIsActive()->willReturn(true);
        $entityManager->getRepository('AppBundle:Member')->willReturn($memberRepository);
        $memberRepository->findBy(Argument::any(), Argument::any(), Argument::any(), Argument::any())
            ->willReturn([$member1, $member2])
        ;
        $people = $this->getPeople();
        $people->shouldEqual([
            'data' => [
                0 => [
                    'id'    => 'person111',
                    'name'  => 'person one',
                    'email' => 'person111@test.com',
                    'isActive' => true,
                ],
                1 => [
                    'id'    => 'person222',
                    'name'  => 'person two',
                    'email' => 'person222@test.com',
                    'isActive' => true,
                ],
            ],
        ]);
    }

    function it_gets_details_for_a_person(EntityManager $entityManager, Member $member)
    {
        $member->getEmail()->willReturn('person111@test.com');
        $member->getId()->willReturn('person111');
        $member->getName()->willReturn('person one');
        $member->getIsActive()->willReturn(true);
        $entityManager->find('AppBundle:Member', 'person111')->willReturn($member);

        $person = $this->getPerson('person111');
        $person->shouldBeArray();
        $person->shouldHaveCount(1);
        $person->shouldEqual([
            'data' => [
                'id' => 'person111',
                'name' => 'person one',
                'email' =>'person111@test.com',
                'isActive' => true
            ]
        ]);
    }
    
    function it_creates_a_new_person(
        EntityManager $entityManager,
        MemberRepository $memberRepository
    )
    {
        $entityManager
            ->getRepository('AppBundle:Member')
            ->willReturn($memberRepository);

        $memberRepository
            ->findBy(['email' => 'person111@test.com'], [], 1)
            ->willReturn(null);
        
        $entityManager->persist(Argument::any())->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $person = $this->createPerson('person one', 'person111@test.com');
        $person->shouldBeArray();
        $person->shouldHaveCount(1);
        $person->shouldHaveKey('data');
        $person['data']->shouldHaveKeyWithValue('name', 'person one');
        $person['data']->shouldHaveKeyWithValue('email', 'person111@test.com');
        $person['data']->shouldHaveKey('id');
    }

    function it_fails_to_create_a_new_person_with_invalid_email() {
        $this->shouldThrow('InvalidArgumentException')->during('createPerson', ['Name', 'invalidemail.com']);
    }

    function it_alters_a_persons_email(
        EntityManager $entityManager,
        MemberRepository $memberRepository,
        Member $member
    )
    {
        $member->getEmail()->willReturn('person111@test.com');
        $member->getId()->willReturn('person111');
        $member->getName()->willReturn('person one');
        $entityManager->getRepository('AppBundle:Member')->willReturn($memberRepository);
        $memberRepository->find('person111')->willReturn($member);
        $memberRepository->findBy(['email' => 'newtest@test.com'])->willReturn(null);

        $member->setEmail('newtest@test.com')->shouldBeCalled();
        $member->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($member)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $this->alterPerson('person111', 'email', 'newtest@test.com')->shouldEqual(true);
    }

    function it_fails_to_alter_an_email_to_an_existing_one(
        EntityManager $entityManager,
        MemberRepository $memberRepository,
        Member $member,
        Member $existingMember
    )
    {
        $member->getEmail()->willReturn('person111@test.com');
        $member->getId()->willReturn('person111');
        $member->getName()->willReturn('person one');
        $existingMember->getEmail()->willReturn('newtest@test.com');
        $existingMember->getId()->willReturn('person222');
        $existingMember->getName()->willReturn('person two');
        $entityManager->getRepository('AppBundle:Member')->willReturn($memberRepository);
        $memberRepository->find('person111')->willReturn($member);
        $memberRepository->findBy(['email' => 'newtest@test.com'])->willReturn([$existingMember]);

        $this->shouldThrow('InvalidArgumentException')->during('alterPerson', ['person111', 'email', 'newtest@test.com']);
    }

    /*
    function it_allows_change_to_blank_email()
    {

    }
    */

    function it_alters_a_persons_name(
        EntityManager $entityManager,
        MemberRepository $memberRepository,
        Member $member
    )
    {
        $member->getEmail()->willReturn('test@test.com');
        $member->getId()->willReturn('person111');
        $member->getName()->willReturn('person one');
        $entityManager->getRepository('AppBundle:Member')->willReturn($memberRepository);
        $memberRepository->find('person111')->willReturn($member);

        $member->setName('new name')->shouldBeCalled();
        $member->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($member)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();
        
        $this->alterPerson('person111', 'name', 'new name')->shouldEqual(true);
    }
    
    function it_doesnt_alter_a_persons_id(
        EntityManager $entityManager,
        MemberRepository $memberRepository,
        Member $member
    )
    {
        $member->getEmail()->willReturn('test@test.com');
        $member->getId()->willReturn('person111');
        $member->getName()->willReturn('person one');
        $entityManager->getRepository('AppBundle:Member')->willReturn($memberRepository);
        $memberRepository->find('person111')->willReturn($member);

        $this->shouldThrow(new \InvalidArgumentException())->during('alterPerson', ['person111', 'id', 'personXXX']);
    }

    function it_throws_exception_getting_an_invalid_person(EntityManager $entityManager)
    {
        $entityManager
            ->find('AppBundle:Member', 'person123')
            ->willReturn(null);
        
        $this->shouldThrow(new PersonNotFoundException())->during('getPerson', ['person123']);
    }

    function it_throws_exception_altering_an_invalid_person(
        EntityManager $entityManager,
        MemberRepository $memberRepository
    )
    {
        $entityManager->getRepository('AppBundle:Member')->willReturn($memberRepository);
        $memberRepository->find('person111')->willReturn(null);

        $this->shouldThrow(new PersonNotFoundException())->during('alterPerson', ['person111', 'anything', 'any value']);
    }

    function it_throws_exception_getting_groups_for_an_invalid_person(EntityManager $entityManager)
    {
        $entityManager
            ->find('AppBundle:Member', 'person123')
            ->willReturn(null);

        $this->shouldThrow(new PersonNotFoundException())->during('getGroups', ['person123']);
    }

    function it_gets_an_empty_array_for_a_person_with_no_groups(
        EntityManager $entityManager,
        GangRepository $gangRepository,
        Member $member
    )
    {
        $member->getId()->willReturn('person123');
        $member->getEmail()->willReturn('test@test.com');
        $member->getName()->willReturn('test');
        $entityManager->find('AppBundle:Member', 'person123')->willReturn($member);
        $entityManager->getRepository('AppBundle:Gang')->willReturn($gangRepository);
        $gangRepository->getGangsForMember($member)->willReturn([]);
        $groups = $this->getGroups('person123');
        $groups->shouldEqual([
            'data' => []
        ]);
    }

    function it_gets_the_groups_that_a_person_belongs_to(
        EntityManager $entityManager,
        GangRepository $gangRepository,
        Member $member,
        Gang $gang1,
        Gang $gang2
    )
    {
        $member->getId()->willReturn('person123');
        $member->getEmail()->willReturn('test@test.com');
        $member->getName()->willReturn('test');
        $gang1->getId()->willReturn('group111');
        $gang1->getName()->willReturn('group one');
        $gang1->getDealType()->willReturn('money');
        $gang2->getId()->willReturn('group222');
        $gang2->getName()->willReturn('group two');
        $gang2->getDealType()->willReturn('simple');
        $entityManager->find('AppBundle:Member', 'person123')->willReturn($member);
        $entityManager->getRepository('AppBundle:Gang')->willReturn($gangRepository);
        $gangRepository->getGangsForMember($member)->willReturn([$gang1, $gang2]);
        $groups = $this->getGroups('person123');
        $groups->shouldEqual([
            'data' => [
                0 => [
                    'id' => 'group111',
                    'name' => 'group one',
                    'type' => 'money'
                ],
                1 => [
                    'id' => 'group222',
                    'name' => 'group two',
                    'type' => 'simple'
                ]
            ]
        ]);
    }

    function it_creates_a_new_group_for_a_person(
        EntityManager $entityManager,
        Member $member
    )
    {
        $member->getId()->willReturn('person111');
        $member->getEmail()->willReturn('person111@test.com');
        $member->getName()->willReturn('test');
        $entityManager->find('AppBundle:Member', 'person111')->willReturn($member);
        $entityManager->persist(Argument::any())->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();
        
        $newGroup = $this->createGroup('person111', 'group name', 'simple');
        $newGroup->shouldBeArray();
        $newGroup->shouldHaveKey('data');
        $newGroup['data']->shouldHaveKeyWithValue('name', 'group name');
        $newGroup['data']->shouldHaveKeyWithValue('type', 'simple');
        $newGroup['data']->shouldHaveKey('id');
    }

    /*
    function it_merges_two_people()
    {
        
    }
    */
    
    function it_doesnt_allow_duplicate_email_addresses(
        EntityManager $entityManager,
        MemberRepository $memberRepository,
        Member $existingMember
    )
    {
        $entityManager
            ->getRepository('AppBundle:Member')
            ->willReturn($memberRepository);
        
        $memberRepository
            ->findBy(['email' => 'person111@test.com'], [], 1)
            ->willReturn($existingMember);

        $this->shouldThrow(new \InvalidArgumentException('A person with this email address already exists'))
            ->during('createPerson', ['Person', 'person111@test.com']);
    }

    function it_can_verify_a_person_can_see_another_persons_details(
        EntityManager $entityManager,
        GangMembershipRepository $gangMembershipRepository
    ) {
        $entityManager
            ->getRepository('AppBundle:GangMembership')
            ->willReturn($gangMembershipRepository);

        $gangMembershipRepository
            ->bothInSameGang('person111', 'person222')
            ->willReturn(true);

        $this->canViewDetails('person111', 'person222')->shouldReturn(true);
    }
}
