<?php

namespace spec\AppBundle\Services;

use AppBundle\Entity\Gang;
use AppBundle\Entity\GangMembership;
use AppBundle\Entity\Member;
use AppBundle\Repository\GangMembershipRepository;
use AppBundle\Repository\GangRepository;
use AppBundle\Repository\MemberRepository;
use AppBundle\Services\GroupsService;
use AppBundle\Services\InviteService;
use Doctrine\ORM\EntityManager;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class GroupsServiceSpec
 * @package spec\AppBundle\Services
 *
 * @mixin GroupsService
 */
class GroupsServiceSpec extends ObjectBehavior
{
    function let(EntityManager $entityManager, InviteService $inviteService)
    {
        $this->beConstructedWith($entityManager, $inviteService);
    }
    
    function it_is_initializable()
    {
        $this->shouldHaveType('AppBundle\Services\GroupsService');
    }
    
    function it_gets_all_groups(
        EntityManager $entityManager,
        GangRepository $gangRepository,
        Gang $gang1,
        Gang $gang2
    )
    {
        $gang1->getId()->willReturn('group111');
        $gang1->getName()->willReturn('group one');
        $gang1->getDealType()->willReturn('simple');
        $gang2->getId()->willReturn('group222');
        $gang2->getName()->willReturn('group two');
        $gang2->getDealType()->willReturn('money');
        
        $entityManager->getRepository('AppBundle:Gang')->willReturn($gangRepository);
        $gangRepository->findBy(Argument::any(), Argument::any(), Argument::any(), Argument::any())
            ->willReturn([$gang1, $gang2])
        ;
        
        $groups = $this->getGroups();
        $groups->shouldEqual([
            'data' => [
                0 => [
                    'id'    => 'group111',
                    'name'  => 'group one',
                    'type' => 'simple',
                ],
                1 => [
                    'id'    => 'group222',
                    'name'  => 'group two',
                    'type' => 'money'
                ]
            ],
        ]);
    }
    
    function it_gets_details_for_a_group(EntityManager $entityManager, Gang $gang)
    {
        $gang->getId()->willReturn('group111');
        $gang->getName()->willReturn('group one');
        $gang->getDealType()->willReturn('simple');
        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);

        $group = $this->getGroup('group111');
        $group->shouldBeArray();
        $group->shouldHaveCount(1);
        $group->shouldEqual([
            'data' => [
                'id' => 'group111',
                'name' => 'group one',
                'type' =>'simple'
            ]
        ]);
    }

    function it_gets_the_people_that_belong_to_it(
        EntityManager $entityManager,
        GangMembershipRepository $gangMembershipRepository,
        Gang $gang,
        Member $member1,
        Member $member2,
        GangMembership $gangMembership1,
        GangMembership $gangMembership2
    )
    {
        $member1->getId()->willReturn('person111');
        $member1->getName()->willReturn('person one');
        $member1->getEmail()->willReturn('person111@test.com');
        $gangMembership1->getMember()->willReturn($member1);
        $gangMembership1->getValue()->willReturn(10.00);
        $gangMembership1->getGang()->willReturn($gang);

        $member2->getId()->willReturn('person222');
        $member2->getName()->willReturn('person two');
        $member2->getEmail()->willReturn('person222@test.com');
        $gangMembership2->getMember()->willReturn($member2);
        $gangMembership2->getValue()->willReturn(-10.00);
        $gangMembership2->getGang()->willReturn($gang);
        
        $gang->getName()->willReturn('group name');
        $gang->getDealType()->willReturn('money');
        $entityManager->find('AppBundle:Gang', 'group123')->willReturn($gang);
        $entityManager->getRepository('AppBundle:GangMembership')->willReturn($gangMembershipRepository);
        $gangMembershipRepository->getGangMembershipsForGang($gang)->willReturn([$gangMembership1, $gangMembership2]);
        $people = $this->getPeople('group123');
        $people->shouldEqual([
            'data' => [
                0 => [
                    'id' => 'person111',
                    'name' => 'person one',
                    'email' => 'person111@test.com',
                    'value' => 10.00
                ],
                1 => [
                    'id' => 'person222',
                    'name' => 'person two',
                    'email' => 'person222@test.com',
                    'value' => -10.00
                ]
            ]
        ]);
    }

    function it_creates_a_new_group()
    {
        $group = $this->createGroup('test group', 'money');
        $group->shouldBeArray();
        $group->shouldHaveCount(1);
        $group->shouldHaveKey('data');
        $group['data']->shouldHaveKeyWithValue('name', 'test group');
        $group['data']->shouldHaveKeyWithValue('type', 'money');
    }
    
    function it_fails_to_create_a_group_with_an_invalid_type()
    {
        $this->shouldThrow(
            new \InvalidArgumentException('wrong is not a valid group type')
        )->during('createGroup', ['name', 'wrong']);
    }

    function it_alters_a_groups_name(EntityManager $entityManager, Gang $gang)
    {
        $gang->getId()->willReturn('group111');
        $gang->getName()->willReturn('group one');
        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);

        $gang->setName('new name')->shouldBeCalled();
        $gang->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($gang)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $this->alterGroup('group111', 'name', 'new name')->shouldEqual(true);
    }
    
    function it_creates_a_new_person_for_a_group(EntityManager $entityManager, InviteService $inviteService, Gang $gang)
    {
        $gang->getId()->willReturn('group111');
        $gang->getName()->willReturn('group one');
        $gang->getDealType()->willReturn('simple');
        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $entityManager->persist(Argument::any())->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();
        $inviteService->sendInvite(Argument::any())->shouldNotBeCalled();
        
        $newPerson = $this->createPerson('group111', 'person one');
        $newPerson->shouldBeArray();
        $newPerson->shouldHaveKey('data');
        $newPerson['data']->shouldHaveKeyWithValue('name', 'person one');
        $newPerson['data']->shouldHaveKey('id');
    }

    function it_does_not_duplicate_a_user_with_an_existing_email_address(
        EntityManager $entityManager,
        MemberRepository $memberRepository,
        InviteService $inviteService,
        Member $existingMember,
        Gang $gang
    ) {
        $gang->getId()->willReturn('group111');
        $gang->getName()->willReturn('group one');
        $gang->getDealType()->willReturn('simple');
        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $entityManager->getRepository('AppBundle:Member')->willReturn($memberRepository);
        $memberRepository->findBy(['email' => 'existing@existing.com'])->willReturn($existingMember);
        $entityManager->persist(Argument::any())->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();
        $inviteService->sendInvite(Argument::any())->shouldNotBeCalled();

        $newPerson = $this->createPerson('group111', 'person one');
        $newPerson->shouldBeArray();
        $newPerson->shouldHaveKey('data');
        $newPerson['data']->shouldHaveKeyWithValue('name', 'person one');
        $newPerson['data']->shouldHaveKey('id');
    }

    function it_removes_a_person_from_a_group(
        EntityManager $entityManager,
        Gang $gang,
        Member $existingMember,
        GangMembership $gangMembership
    ) {
        $gangMembership->getGang()->willReturn($gang);
        $gangMembership->getMember()->willReturn($existingMember);
        $gangMembership->getValue()->willReturn(0.00);
        $gangMembership->setDeletedAt(Argument::type('DateTime'))->shouldBeCalled();

        $existingMember->getIsActive()->willReturn(true);
        $existingMember->getGangMemberships()->willReturn([$gangMembership]);
        $entityManager->persist($gangMembership)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();
        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $entityManager->find('AppBundle:Member', 'person111')->willReturn($existingMember);

        $this->removePerson('group111', 'person111');
    }

    function it_removes_a_person_completely_if_they_are_not_active(
        EntityManager $entityManager,
        Gang $gang,
        Member $existingMember,
        GangMembership $gangMembership
    ) {
        $gangMembership->getGang()->willReturn($gang);
        $gangMembership->getMember()->willReturn($existingMember);
        $gangMembership->getValue()->willReturn(0.00);
        $gangMembership->setDeletedAt(Argument::type('DateTime'))->shouldBeCalled();

        $existingMember->getIsActive()->willReturn(false);
        $existingMember->getGangMemberships()->willReturn([$gangMembership]);
        $existingMember->setDeletedAt(Argument::type('DateTime'))->shouldBeCalled();
        $entityManager->persist($gangMembership)->shouldBeCalled();
        $entityManager->persist($existingMember)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();
        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $entityManager->find('AppBundle:Member', 'person111')->willReturn($existingMember);

        $this->removePerson('group111', 'person111');
    }

    function it_fails_to_remove_a_person_from_a_group_if_they_are_not_zero(
        EntityManager $entityManager,
        Gang $gang,
        Member $existingMember,
        GangMembership $gangMembership
    ) {
        $gangMembership->getGang()->willReturn($gang);
        $gangMembership->getMember()->willReturn($existingMember);
        $gangMembership->getValue()->willReturn(0.50);
        $gangMembership->setDeletedAt(Argument::type('DateTime'))->shouldNotBeCalled();

        $existingMember->getIsActive()->willReturn(true);
        $existingMember->getGangMemberships()->willReturn([$gangMembership]);
        $entityManager->persist($gangMembership)->shouldNotBeCalled();
        $entityManager->flush()->shouldNotBeCalled();
        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $entityManager->find('AppBundle:Member', 'person111')->willReturn($existingMember);

        $this->shouldThrow(new \Exception('Can not remove a person from a group if their value is not 0'))->during('removePerson', ['group111', 'person111']);
    }
    
    function it_sends_an_invite_if_email_is_supplied(
        EntityManager $entityManager,
        MemberRepository $memberRepository,
        InviteService $inviteService,
        Gang $gang
    ) {
        $gang->getId()->willReturn('group111');
        $gang->getName()->willReturn('group one');
        $gang->getDealType()->willReturn('simple');
        $entityManager->find('AppBundle:Gang', 'group111')->willReturn($gang);
        $entityManager->getRepository('AppBundle:Member')->willReturn($memberRepository);
        $entityManager->persist(Argument::any())->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();
        $inviteService->sendInvite(Argument::any())->shouldBeCalled();
        
        $newPerson = $this->createPerson('group111', 'person one', 'person111@test.com');
        $newPerson->shouldBeArray();
        $newPerson->shouldHaveKey('data');
        $newPerson['data']->shouldHaveKeyWithValue('name', 'person one');
        $newPerson['data']->shouldHaveKeyWithValue('email', 'person111@test.com');
        $newPerson['data']->shouldHaveKey('id');
    }

    /*
    function it_links_an_existing_person(EntityManager $entityManager, Gang $gang, Member $existingMember)
    {
        
    }
    
    function it_sends_an_invite_when_linking_an_existing_person_with_an_email()
    {
        
    }
    */

    function it_adjusts_correctly_from_simple_to_money(
        EntityManager $entityManager,
        GangRepository $gangRepository,
        Gang $gang,
        GangMembership $damian,
        GangMembership $mikey,
        GangMembership $sally,
        GangMembership $leo
    )
    {
        $gang->getId()->willReturn('group111');
        $gang->getName()->willReturn('group one');
        $gang->getDealType()->willReturn('simple');

        $damian->getValue()->willReturn(4);
        $mikey->getValue()->willReturn(1);
        $sally->getValue()->willReturn(-2);
        $leo->getValue()->willReturn(-3);

        $entityManager->getRepository('AppBundle:Gang')->willReturn($gangRepository);
        $gangRepository->find('group111')->willReturn($gang);
        $gang->getGangMemberships()->willReturn([$damian, $mikey, $sally, $leo]);
        $gang->setDealType('money')->shouldBeCalled();
        $damian->setValue(Argument::any())->shouldNotBeCalled();
        $damian->setUpdatedAt(Argument::any())->shouldNotBeCalled();
        $entityManager->persist($damian)->shouldNotBeCalled();

        $gang->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($gang)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $this->switchType('group111', 'money');
    }

    /**
     * damian: 4, mikey: 1, sally: -2, leo: -3
     * needs to be given a multiplier i.e. if they've mostly been buying coffee it might be 4.5, beer might be 9
     * using 4.50 (coffee): damian: 18.00, mikey: 4.50, sally: -9.00, leo: -13.50
     * using 9.00 (beer): damian: 36.00, mikey: 9.00, sally: -18.00, leo: -27.00
     */
    function it_adjusts_correctly_from_simple_to_money_when_given_a_factor(
        EntityManager $entityManager,
        GangRepository $gangRepository,
        Gang $gang,
        GangMembership $damian,
        GangMembership $mikey,
        GangMembership $sally,
        GangMembership $leo
    )
    {
        $gang->getId()->willReturn('group111');
        $gang->getName()->willReturn('group one');
        $gang->getDealType()->willReturn('simple');

        $damian->getValue()->willReturn(4);
        $mikey->getValue()->willReturn(1);
        $sally->getValue()->willReturn(-2);
        $leo->getValue()->willReturn(-3);

        $entityManager->getRepository('AppBundle:Gang')->willReturn($gangRepository);
        $gangRepository->find('group111')->willReturn($gang);
        $gang->getGangMemberships()->willReturn([$damian, $mikey, $sally, $leo]);
        $gang->setDealType('money')->shouldBeCalled();
        $damian->setValue(18.00)->shouldBeCalled();
        $damian->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($damian)->shouldBeCalled();
        $mikey->setValue(4.50)->shouldBeCalled();
        $mikey->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($mikey)->shouldBeCalled();
        $sally->setValue(-9.00)->shouldBeCalled();
        $sally->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($sally)->shouldBeCalled();
        $leo->setValue(-13.50)->shouldBeCalled();
        $leo->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($leo)->shouldBeCalled();

        $gang->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($gang)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $this->switchType('group111', 'money', 4.50);
    }

    /**
     * damian: 100.00, mikey: 25.50, sally: -40.75, leo: -84.75
     * to convert, take the value closest to 0 and reduce to 1 i.e. 1/25.50 = 0.039215686
     * now multiply the others and round off. all should balance to zero
     * damian: 4, mikey: 1, sally: -2, leo: -3
     */
    function it_adjusts_correctly_from_money_to_simple(
        EntityManager $entityManager,
        GangRepository $gangRepository,
        Gang $gang,
        GangMembership $damian,
        GangMembership $mikey,
        GangMembership $sally,
        GangMembership $leo
    )
    {
        $gang->getId()->willReturn('group111');
        $gang->getName()->willReturn('group one');
        $gang->getDealType()->willReturn('money');

        $damian->getValue()->willReturn(100.00);
        $mikey->getValue()->willReturn(25.50);
        $sally->getValue()->willReturn(-40.75);
        $leo->getValue()->willReturn(-84.75);

        $entityManager->getRepository('AppBundle:Gang')->willReturn($gangRepository);
        $gangRepository->find('group111')->willReturn($gang);
        $gang->getGangMemberships()->willReturn([$damian, $mikey, $sally, $leo]);
        $gang->setDealType('simple')->shouldBeCalled();
        $damian->setValue(4.00)->shouldBeCalled();
        $damian->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($damian)->shouldBeCalled();
        $mikey->setValue(1.00)->shouldBeCalled();
        $mikey->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($mikey)->shouldBeCalled();
        $sally->setValue(-2.00)->shouldBeCalled();
        $sally->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($sally)->shouldBeCalled();
        $leo->setValue(-3.00)->shouldBeCalled();
        $leo->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($leo)->shouldBeCalled();

        $gang->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($gang)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $this->switchType('group111', 'simple');
    }
    
    function it_adjusts_correctly_from_money_to_simple_when_given_a_factor(
        EntityManager $entityManager,
        GangRepository $gangRepository,
        Gang $gang,
        GangMembership $damian,
        GangMembership $nicci,
        GangMembership $sally
    )
    {
        $gang->getId()->willReturn('group111');
        $gang->getName()->willReturn('group one');
        $gang->getDealType()->willReturn('money');
        
        $damian->getValue()->willReturn(100.00);
        $nicci->getValue()->willReturn(-2.00);
        $sally->getValue()->willReturn(-98.00);

        $entityManager->getRepository('AppBundle:Gang')->willReturn($gangRepository);
        $gangRepository->find('group111')->willReturn($gang);
        $gang->getGangMemberships()->willReturn([$damian, $nicci, $sally]);
        $gang->setDealType('simple')->shouldBeCalled();
        $damian->setValue(11.00)->shouldBeCalled();
        $damian->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($damian)->shouldBeCalled();
        $nicci->setValue(0.00)->shouldBeCalled();
        $nicci->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($nicci)->shouldBeCalled();
        $sally->setValue(-11.00)->shouldBeCalled();
        $sally->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($sally)->shouldBeCalled();

        $gang->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($gang)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $this->switchType('group111', 'simple', 9.00);
    }

    // TODO: this is borked
    function _it_handles_split_rounding_by_forgiving_debt_most_to_least(
        EntityManager $entityManager,
        GangRepository $gangRepository,
        Gang $gang,
        GangMembership $damian,
        GangMembership $nicci,
        GangMembership $sally
    )
    {
        $gang->getId()->willReturn('group111');
        $gang->getName()->willReturn('group one');
        $gang->getDealType()->willReturn('money');

        $damian->getValue()->willReturn(100.00);
        $nicci->getValue()->willReturn(-3.00);
        $sally->getValue()->willReturn(-97.00);

        $entityManager->getRepository('AppBundle:Gang')->willReturn($gangRepository);
        $gangRepository->find('group111')->willReturn($gang);
        $gang->getGangMemberships()->willReturn([$damian, $nicci, $sally]);
        $gang->setDealType('simple')->shouldBeCalled();
        $damian->setValue(50.00)->shouldBeCalled();
        $damian->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($damian)->shouldBeCalled();
        $nicci->setValue(-2.00)->shouldBeCalled();
        $nicci->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($nicci)->shouldBeCalled();
        $sally->setValue(-48.00)->shouldBeCalled();
        $sally->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($sally)->shouldBeCalled();

        $gang->setUpdatedAt(Argument::any())->shouldBeCalled();
        $entityManager->persist($gang)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $this->switchType('group111', 'simple', 2.00);
    }

    /*
    function it_allows_all_people_in_a_group_to_be_zeroed()
    {

    }
    */

    function it_can_verify_a_person_can_see_a_groups_details(
        EntityManager $entityManager,
        GangMembershipRepository $gangMembershipRepository
    )
    {
        $entityManager
            ->getRepository('AppBundle:GangMembership')
            ->willReturn($gangMembershipRepository);

        $gangMembershipRepository
            ->isInGang('person111', 'group111')
            ->willReturn(true);

        $this->canViewDetails('person111', 'group111')->shouldReturn(true);
    }
}
