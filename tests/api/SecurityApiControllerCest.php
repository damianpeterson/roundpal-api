<?php


class SecurityApiControllerCest
{
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    public function signUpWithNewEmail(ApiTester $I)
    {
        $I->wantTo('As a new person I would like to be able to sign up with my email address');

        $I->sendPOST('/sign-up-in', json_encode([
            'email' => 'person111@test.com'
        ]));

        $I->canSeeResponseCodeIs(201);

        // TODO: make sure an email is sent
    }

    public function failToSignUpWithAnInvalidEmail(ApiTester $I)
    {
        $I->wantTo('As a new Person signing up I should not be able to use an invalid email address');

        $I->sendPOST('/sign-up-in', json_encode([
            'email' => 'person111test.com'
        ]));

        $I->canSeeResponseCodeIs(400);
    }

    public function createGroupForFirstTimeSignUp(ApiTester $I)
    {
        $I->wantTo('As a new Person I want my first group created for me so I don\'t have to bother with it initially');

        $I->sendPOST('/sign-up-in', json_encode([
            'email' => 'person111@test.com'
        ]));

        $I->canSeeResponseCodeIs(201);
        $I->canSeeInDatabase('members', ['email' => 'person111@test.com']);
        $I->canSeeInDatabase('gangs', ['name' => 'My Group']);
    }

    public function signUpWithExistingEmail(ApiTester $I)
    {
        $I->wantTo('As an existing Person I would like to sign in with my email address');

        $I->addMember('person111');
        $I->sendPOST('/sign-up-in', json_encode([
            'email' => 'person111@test.com'
        ]));

        $I->canSeeResponseCodeIs(200);

        // TODO: make sure an email is sent
    }

    public function getTokenForCode(ApiTester $I)
    {
        $I->wantTo('As a Person who has received a sign in email I would like to be able to get my api token');

        $I->addMember('person111', 'person111_code', new \DateTime('now'));
        $I->sendGET('/sign-in/person111/person111_code');

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseContains('person111_token');
    }

    public function failToGetTokenForExpiredCode(ApiTester $I)
    {
        $I->wantTo('As a Person who has received a sign in email I should not be able to get a token if the code has expired');

        $I->addMember('person111', 'person111_code', new \DateTime('-2 day'));
        $I->sendGET('/sign-in/person111/person111_code');

        $I->canSeeResponseCodeIs(401);
    }

    public function failToGetTokenForMissingCode(ApiTester $I)
    {
        $I->wantTo('As a Person attempting to get a token I should not get one if there is no code');

        $I->addMember('person111');
        $I->sendGET('/sign-in/person111/person111_code');

        $I->canSeeResponseCodeIs(401);
    }

    public function failToGetTokenForWrong(ApiTester $I)
    {
        $I->wantTo('As a Person attempting to get a token I should not get one if my code is wrong');

        $I->addMember('person111', 'person222_code');
        $I->sendGET('/sign-in/person111/person111_code');

        $I->canSeeResponseCodeIs(401);
    }
}
