<?php


class PeopleApiControllerCest
{
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    public function smokeTestAsNoToken(ApiTester $I)
    {
        $I->wantTo('As a request with no token I should not be able to access these endpoints');

        $I->sendGET('/people');
        $I->canSeeResponseCodeIs(403);

        $I->sendGET('/people/person111');
        $I->canSeeResponseCodeIs(403);

        $I->sendPOST('/people', json_encode([]));
        $I->canSeeResponseCodeIs(403);

        $I->sendPATCH('/people/person111', json_encode([]));
        $I->canSeeResponseCodeIs(403);

        $I->sendGET('/groups/group111/people');
        $I->canSeeResponseCodeIs(403);

        $I->sendPOST('/groups/group111/people', json_encode([]));
        $I->canSeeResponseCodeIs(403);

        $I->sendDELETE('/groups/group111/people/person111');
        $I->canSeeResponseCodeIs(403);
    }

    public function smokeTestAsInvalidToken(ApiTester $I)
    {
        $I->wantTo('As a request with an invalid token I should not be able to access these endpoints');

        $I->haveHttpHeader('X-Api-Key', 'invalid_token');

        $I->sendGET('/people');
        $I->canSeeResponseCodeIs(403);

        $I->sendGET('/people/person111');
        $I->canSeeResponseCodeIs(403);

        $I->sendPOST('/people', json_encode([]));
        $I->canSeeResponseCodeIs(403);

        $I->sendPATCH('/people/person111', json_encode([]));
        $I->canSeeResponseCodeIs(403);

        $I->sendGET('/groups/group111/people');
        $I->canSeeResponseCodeIs(403);

        $I->sendPOST('/groups/group111/people', json_encode([]));
        $I->canSeeResponseCodeIs(403);

        $I->sendDELETE('/groups/group111/people/person111');
        $I->canSeeResponseCodeIs(403);
    }

    public function smokeTestAsValidNonAdminToken(ApiTester $I)
    {
        $I->wantTo('As a request with an valid non admin token I should not be able to access these endpoints');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');
        $I->addMember('person111');

        $I->sendGET('/people');
        $I->canSeeResponseCodeIs(403);

        $I->sendPOST('/people', json_encode([]));
        $I->canSeeResponseCodeIs(403);
    }

    public function getPeopleAsAdmin(ApiTester $I)
    {
        // http://matthewturland.com/slides/codeception/#title-slide
        $I->wantTo('As an administrator I would like to see a list of all people');

        $I->haveHttpHeader('X-Api-Key', 'admin_token');
        $I->addAdmin();
        $I->addMember('person111');
        $I->addMember('person222');
        $I->sendGET('/people');
        
        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->canSeeResponseContainsJson(['name' => 'nameperson111', 'email' => 'person111@test.com']);
        $I->canSeeResponseContainsJson(['name' => 'nameperson222', 'email' => 'person222@test.com']);
    }

    public function getPaginatedPeopleAsAdmin(ApiTester $I)
    {
        $I->wantTo('As an administrator I would like to see a paginated list of all people');

        $I->haveHttpHeader('X-Api-Key', 'admin_token');
        $I->addAdmin();

        for ($i = 1001; $i <= 1120; $i++) {
            $I->addMember('person' . $i);
        }

        // get all results from a small set of data
        $I->sendGET('/people');

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->canSeeResponseContainsJson(['name' => 'nameperson1001']);
        $I->canSeeResponseContainsJson(['name' => 'nameperson1100']);
        $I->cantSeeResponseContainsJson(['name' => 'nameperson1101']);
        $I->cantSeeResponseContainsJson(['name' => 'nameperson1120']);

        // get the first page of 2 results
        $I->sendGET('/people?offset=0&limit=2');

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->canSeeResponseContainsJson(['name' => 'nameperson1001']);
        $I->canSeeResponseContainsJson(['name' => 'nameperson1002']);
        $I->cantSeeResponseContainsJson(['name' => 'nameperson1003']);
        $I->cantSeeResponseContainsJson(['name' => 'nameperson1200']);
        
        // now try a few pages on
        $I->sendGET('/people?offset=100&limit=20');

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->cantSeeResponseContainsJson(['name' => 'nameperson1001']);
        $I->cantSeeResponseContainsJson(['name' => 'nameperson1100']);
        $I->canSeeResponseContainsJson(['name' => 'nameperson1101']);
        $I->canSeeResponseContainsJson(['name' => 'nameperson1120']);

        // we should only get what's left when we ask for beyond the end of the data set
        $I->sendGET('/people?offset=119&limit=100');

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->cantSeeResponseContainsJson(['name' => 'nameperson1001']);
        $I->cantSeeResponseContainsJson(['name' => 'nameperson1119']);
        $I->canSeeResponseContainsJson(['name' => 'nameperson1120']);
    }

    public function getPersonDetailsForAPersonInSameGroup(ApiTester $I)
    {
        $I->wantTo('As a Person in the same Group I would like to see a Person\'s details');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');

        $I->addGang('group111');
        $I->addMember('person111');
        $I->addMember('person222');
        $I->addGangMembership(1, 'person111', 'group111', 10);
        $I->addGangMembership(2, 'person222', 'group111', -10);
        $I->sendGET('/people/person222');

        $I->canSeeResponseCodeIs(200);
    }

    public function failToGetPersonDetailsForAPersonNotInSameGroup(ApiTester $I)
    {
        $I->wantTo('As a Person not in the same Group I should not be able to see a Person\'s details');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');

        $I->addGang('group111');
        $I->addGang('group222');
        $I->addMember('person111');
        $I->addMember('person222');
        $I->addGangMembership(1, 'person111', 'group111', 10);
        $I->addGangMembership(2, 'person222', 'group222', -10);
        $I->sendGET('/people/person222');

        $I->canSeeResponseCodeIs(404);
    }

    public function alterANonActivePersonsDetails(ApiTester $I)
    {
        $I->wantTo('As an active Person from the same Group I would like to be able to alter a non active Person\'s name or email');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');
        $I->addMember('person111');
        $I->addMember('person222');
        $I->addGang('group111');
        $I->addGangMembership(1, 'person111', 'group111');
        $I->addGangMembership(2, 'person222', 'group111');

        $I->sendPATCH('/people/person222', json_encode([
            'op' => 'replace',
            'path' => 'name',
            'value' => 'New Name',
        ]));

        $I->canSeeResponseCodeIs(200);
    }

    public function failToAlterAPersonsDetailsIfYouDontBelongToTheSameGroup(ApiTester $I)
    {
        $I->wantTo('As an active Person not in the same Group I should not be able to alter a Person\'s name or email');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');
        $I->addMember('person111');
        $I->addMember('person222');
        $I->addGang('group111');
        $I->addGang('group222');
        $I->addGangMembership(1, 'person111', 'group111');
        $I->addGangMembership(2, 'person222', 'group222');

        $I->sendPATCH('/people/person222', json_encode([
            'op' => 'replace',
            'path' => 'name',
            'value' => 'New Name',
        ]));

        $I->canSeeResponseCodeIs(404);
    }

    public function sendAnotherInviteWhenChangingNonActivePersonsEmail(ApiTester $I, $scenario)
    {
        $I->wantTo('As an active Person in the same Group I would like a new invite email to be sent when I change the email of a non active Person');

        $scenario->incomplete();
    }

    public function createPerson(ApiTester $I)
    {
        $I->wantTo('As an administrator I would like to be able to create a new person');

        $I->haveHttpHeader('X-Api-Key', 'admin_token');
        $I->addAdmin();

        $I->sendPOST('/people', json_encode([
            'name' => 'Person',
            'email' => 'person111@test.com'
        ]));

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->seeInDatabase('members', ['name' => 'Person', 'email' => 'person111@test.com']);
    }

    public function mergeTwoPeople(ApiTester $I, $scenario)
    {
        $I->wantTo('As a Person with two accounts I would like to be able to merge them so I don\'t have to keep '.
            'logging in with different accounts');
        
        $scenario->incomplete();
    }

    public function failToCreateAPersonWithoutAName(ApiTester $I)
    {
        $I->wantTo('I should not be able to create a new person without a name');

        $I->haveHttpHeader('X-Api-Key', 'admin_token');
        $I->addAdmin();

        $I->addMember('person111');
        
        $I->sendPOST('/people', json_encode([
            'email' => 'person111'
        ]));

        $I->canSeeResponseCodeIs(400);
    }

    public function failToCreateAPersonWithoutAnEmailAddress(ApiTester $I)
    {
        $I->wantTo('I should not be able to create a new person without an email address');

        $I->haveHttpHeader('X-Api-Key', 'admin_token');
        $I->addAdmin();

        $I->addMember('person111');
        
        $I->sendPOST('/people', json_encode([
            'name' => 'Person'
        ]));

        $I->canSeeResponseCodeIs(400);
    }

    public function failToCreateAPersonWithAnExistingEmailAddress(ApiTester $I)
    {
        $I->addMember('person111');

        $I->haveHttpHeader('X-Api-Key', 'admin_token');
        $I->addAdmin();

        $I->wantTo('I should not be able to create a new person with an email that already exists');
        $I->sendPOST('/people', json_encode([
            'name' => 'Person',
            'email' => 'person111@test.com'
        ]));
        
        $I->canSeeResponseCodeIs(400);
    }

    public function createAPersonForAGroup(ApiTester $I)
    {
        $I->wantTo('As an active member of a group I would like to be able to create a new person for a group');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');
        $I->addMember('person111');
        $I->addGang('group111');
        $I->addGangMembership(1, 'person111', 'group111');

        $I->sendPOST('/groups/group111/people', json_encode([
            'name' => 'Person'
        ]));

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->canSeeInDatabase('members', ['name' => 'Person']);
    }

    public function failToCreateAPersonForAGroupThatIDontBelongTo(ApiTester $I)
    {
        $I->wantTo('As a Person who doesn\'t belong to this Group I should not be able to create a new Person in it');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');
        $I->addMember('person111');
        $I->addGang('group111');
        $I->addGang('group222');
        $I->addGangMembership(1, 'person111', 'group222');

        $I->sendPOST('/groups/group111/people', json_encode([
            'name' => 'Person'
        ]));

        $I->canSeeResponseCodeIs(404);
    }

    public function deleteAPersonFromAGroup(ApiTester $I)
    {
        $I->wantTo('As an active member of a group I would like to be able to delete a person from the group');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');
        $I->addMember('person111');
        $now = new \DateTime('now');
        $I->haveInDatabase('members', [
            'id' => 'person222',
            'name' => 'nameperson222',
            'email' => 'person222@test.com',
            'token' => 'person222_token',
            'created_at' => $now->format('Y-m-d H:i:s'),
            'updated_at' => $now->format('Y-m-d H:i:s'),
            'last_signed_in_at' => $now->format('Y-m-d H:i:s')
        ]);
        $I->addGang('group111');
        $I->addGangMembership(1, 'person111', 'group111');
        $I->addGangMembership(2, 'person222', 'group111');

        $I->sendDELETE('/groups/group111/people/person222');

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $membershipDeletedAt = $I->grabFromDatabase('gang_memberships', 'deleted_at', ['id' => 2]);
        $memberDeletedAt = $I->grabFromDatabase('members', 'deleted_at', ['id' => 'person222']);
        $I->assertFalse($membershipDeletedAt === null);
        $I->assertTrue($memberDeletedAt === null);
    }

    public function deleteAPersonFromAGroupAsWellAsFromPeople(ApiTester $I)
    {
        $I->wantTo('As an active member of a group I would like to be able to delete a person from the group and from the people if they are not active');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');
        $I->addMember('person111');
        $I->addMember('person222');
        $I->addGang('group111');
        $I->addGangMembership(1, 'person111', 'group111');
        $I->addGangMembership(2, 'person222', 'group111');

        $I->sendDELETE('/groups/group111/people/person222');

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $membershipDeletedAt = $I->grabFromDatabase('gang_memberships', 'deleted_at', ['id' => 2]);
        $memberDeletedAt = $I->grabFromDatabase('members', 'deleted_at', ['id' => 'person222']);
        $I->assertFalse($membershipDeletedAt === null);
        $I->assertFalse($memberDeletedAt === null);
    }

    public function sendInviteToNewPerson(ApiTester $I, $scenario)
    {
        $I->wantTo('As an active Person in a Group I would like an email to be sent when I create a new Person with an email address');

        $scenario->incomplete();
    }
}
