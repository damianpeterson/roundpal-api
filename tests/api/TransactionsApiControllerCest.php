<?php


class TransactionsApiControllerCest
{
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    public function getTransactionsForAGroup(ApiTester $I, $scenario)
    {
        $I->wantTo('As a member of a Group I would like to be able to see its Transactions');
        $scenario->incomplete();
    }

    public function createTransactionForAGroup(ApiTester $I, $scenario)
    {
        $I->wantTo('As a member of a Group I would like to be able to create a new Transaction');
        $scenario->incomplete();
    }

    public function reverseATransaction(ApiTester $I, $scenario)
    {
        $I->wantTo('As a member of a Group I would like to be able to reverse an existing Transaction');
        $scenario->incomplete();
    }
}
