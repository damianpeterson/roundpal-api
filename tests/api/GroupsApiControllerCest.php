<?php


class GroupsApiControllerCest
{
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    public function smokeTestAsNoToken(ApiTester $I)
    {
        $I->wantTo('As a request with no token I should not be able to access these endpoints');

        $I->sendGET('/groups');
        $I->canSeeResponseCodeIs(403);

        $I->sendGET('/groups/person111');
        $I->canSeeResponseCodeIs(403);

        $I->sendPOST('/groups', json_encode([]));
        $I->canSeeResponseCodeIs(403);

        $I->sendPATCH('/groups/group111', json_encode([]));
        $I->canSeeResponseCodeIs(403);

        $I->sendGET('/people/person111/groups');
        $I->canSeeResponseCodeIs(403);

        $I->sendPOST('/people/person111/groups', json_encode([]));
        $I->canSeeResponseCodeIs(403);
    }

    public function smokeTestAsInvalidToken(ApiTester $I)
    {
        $I->wantTo('As a request with an invalid token I should not be able to access these endpoints');

        $I->haveHttpHeader('X-Api-Key', 'invalid_token');

        $I->sendGET('/groups');
        $I->canSeeResponseCodeIs(403);

        $I->sendGET('/groups/person111');
        $I->canSeeResponseCodeIs(403);

        $I->sendPOST('/groups', json_encode([]));
        $I->canSeeResponseCodeIs(403);

        $I->sendPATCH('/groups/group111', json_encode([]));
        $I->canSeeResponseCodeIs(403);

        $I->sendGET('/people/person111/groups');
        $I->canSeeResponseCodeIs(403);

        $I->sendPOST('/people/person111/groups', json_encode([]));
        $I->canSeeResponseCodeIs(403);
    }

    public function smokeTestAsValidNonAdminToken(ApiTester $I)
    {
        $I->wantTo('As a request with an valid non admin token I should not be able to access these endpoints');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');
        $I->addMember('person111');

        $I->sendGET('/groups');
        $I->canSeeResponseCodeIs(403);

        $I->sendPOST('/groups', json_encode([]));
        $I->canSeeResponseCodeIs(403);
    }

    public function getGroups(ApiTester $I)
    {
        $I->wantTo('As an administrator I would like to see a list of all groups');

        $I->haveHttpHeader('X-Api-Key', 'admin_token');
        $I->addAdmin();

        $I->addGang('group111');
        $I->addGang('group222', 'money');
        $I->sendGET('/groups');
        
        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->canSeeResponseContainsJson(['name' => 'namegroup111', 'type' => 'simple']);
        $I->canSeeResponseContainsJson(['name' => 'namegroup222', 'type' => 'money']);
    }

    public function getPaginatedGroups(ApiTester $I)
    {
        // http://matthewturland.com/slides/codeception/#title-slide
        $I->wantTo('As an administrator I would like to see a paginated list of all groups');

        $I->haveHttpHeader('X-Api-Key', 'admin_token');
        $I->addAdmin();

        for ($i = 1001; $i <= 1120; $i++) {
            $I->addGang('group' . $i);
        }
        
        // get all results from a small set of data
        $I->sendGET('/groups');

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->canSeeResponseContainsJson(['name' => 'namegroup1001']);
        $I->canSeeResponseContainsJson(['name' => 'namegroup1100']);
        $I->cantSeeResponseContainsJson(['name' => 'namegroup1101']);
        $I->cantSeeResponseContainsJson(['name' => 'namegroup1120']);

        // get the first page of 2 results
        $I->sendGET('/groups?offset=0&limit=2');

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->canSeeResponseContainsJson(['name' => 'namegroup1001']);
        $I->canSeeResponseContainsJson(['name' => 'namegroup1002']);
        $I->cantSeeResponseContainsJson(['name' => 'namegroup1003']);
        $I->cantSeeResponseContainsJson(['name' => 'namegroup1200']);
        
        // now try a few pages on
        $I->sendGET('/groups?offset=100&limit=20');

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->cantSeeResponseContainsJson(['name' => 'namegroup1001']);
        $I->cantSeeResponseContainsJson(['name' => 'namegroup1100']);
        $I->canSeeResponseContainsJson(['name' => 'namegroup1101']);
        $I->canSeeResponseContainsJson(['name' => 'namegroup1120']);

        // we should only get what's left when we ask for beyond the end of the data set
        $I->sendGET('/groups?offset=119&limit=100');

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->cantSeeResponseContainsJson(['name' => 'namegroup1001']);
        $I->cantSeeResponseContainsJson(['name' => 'namegroup1119']);
        $I->canSeeResponseContainsJson(['name' => 'namegroup1120']);
    }
    
    public function getGroup(ApiTester $I)
    {
        $I->wantTo('As a Person who belongs to a Group I should be able to see the Group details');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');
        $I->addMember('person111');
        $I->addGang('group111');
        $I->addGangMembership(1, 'person111', 'group111');

        $I->sendGET('/groups/group111');

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->canSeeResponseContainsJson(['name' => 'namegroup111', 'type' => 'simple']);
    }

    public function failToGetGroup(ApiTester $I)
    {
        $I->wantTo('As a Person who does not belong to a Group I should not be able to see the Group details');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');
        $I->addMember('person111');
        $I->addGang('group111');
        $I->addGang('group222');
        $I->addGangMembership(1, 'person111', 'group111');

        $I->sendGET('/groups/group222');

        $I->canSeeResponseCodeIs(404);
    }

    public function alterTheGroupsName(ApiTester $I)
    {
        $I->wantTo('As a member of this Group I should be able to change its name');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');
        $I->addMember('person111');
        $I->addGang('group111');
        $I->addGangMembership(1, 'person111', 'group111');

        $I->sendPATCH('/groups/group111', json_encode([
            'op' => 'replace',
            'path' => 'name',
            'value' => 'New Name',
        ]));

        $I->canSeeResponseCodeIs(200);
    }

    public function failToAlterTheGroupsNameIfYouDontBelongToIt(ApiTester $I)
    {
        $I->wantTo('As a Person who belongs to a different Group I should not be able to rename this Group');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');
        $I->addMember('person111');
        $I->addGang('group111');
        $I->addGang('group222');
        $I->addGangMembership(1, 'person111', 'group111');

        $I->sendPATCH('/groups/group222', json_encode([
            'op' => 'replace',
            'path' => 'name',
            'value' => 'New Name',
        ]));

        $I->canSeeResponseCodeIs(404);
    }

    public function createGroup(ApiTester $I)
    {
        $I->wantTo('As an admin user I would like to be able to create a new Group');

        $I->haveHttpHeader('X-Api-Key', 'admin_token');
        $I->addAdmin();

        $I->sendPOST('/groups', json_encode([
            'name' => 'my new group',
            'type' => 'simple'
        ]));

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->seeInDatabase('gangs', ['name' => 'my new group', 'deal_type' => 'simple']);
    }

    public function failToCreateGroupAsNonAdmin(ApiTester $I)
    {
        $I->wantTo('As a non-admin user I should not be able to create a group');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');
        $I->addMember('person111');

        $I->sendPOST('/groups', json_encode([
            'name' => 'my new group',
            'type' => 'simple'
        ]));

        $I->canSeeResponseCodeIs(403);
    }

    public function failToCreateAGroupWithoutAName(ApiTester $I)
    {
        $I->wantTo('As an admin I should not be able to create a new group without a name');

        $I->haveHttpHeader('X-Api-Key', 'admin_token');
        $I->addAdmin();

        $I->sendPOST('/groups', json_encode([
            'type' => 'simple'
        ]));

        $I->canSeeResponseCodeIs(400);
    }

    public function failToCreateAGroupWithoutAType(ApiTester $I)
    {
        $I->wantTo('As an admin I should not be able to create a new group without a type');

        $I->haveHttpHeader('X-Api-Key', 'admin_token');
        $I->addAdmin();

        $I->sendPOST('/groups', json_encode([
            'name' => 'my new group'
        ]));

        $I->canSeeResponseCodeIs(400);
    }

    public function createAGroupForAPerson(ApiTester $I)
    {
        $I->wantTo('As an authorised user I would like to create a new Group');

        $I->haveHttpHeader('X-Api-Key', 'person111_token');
        $I->addMember('person111');

        $I->sendPOST('/people/person111/groups', json_encode([
            'name' => 'My New Group',
            'type' => 'simple'
        ]));

        $I->canSeeResponseCodeIs(200);
        $I->canSeeResponseIsJson();
        $I->canSeeInDatabase('gangs', ['name' => 'My New Group', 'deal_type' => 'simple']);
    }
}
