<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class ApiTester extends \Codeception\Actor
{
    use _generated\ApiTesterActions;

    public function addAdmin()
    {
        $now = new \DateTime('tomorrow');
        $this->haveInDatabase('members', [
            'id' => '11111111-1111-1111-1111-111111111111',
            'name' => 'Admin',
            'email' => 'admin@roundpal.com',
            'token' => 'admin_token',
            'created_at' => $now->format('Y-m-d H:i:s'),
            'updated_at' => $now->format('Y-m-d H:i:s')
        ]);
    }

    public function addMember($id, $code = null, DateTime $codeRequestedAt = null)
    {
        $now = new \DateTime('now');
        $this->haveInDatabase('members', [
            'id' => $id,
            'name' => 'name' .  $id,
            'email' => $id . '@test.com',
            'token' => $id . '_token',
            'code' => $code,
            'code_requested_at' => $codeRequestedAt !== null ? $codeRequestedAt->format('Y-m-d H:i:s') : null,
            'created_at' => $now->format('Y-m-d H:i:s'),
            'updated_at' => $now->format('Y-m-d H:i:s')
        ]);
    }

    public function addGang($id, $type = 'simple')
    {
        $now = new \DateTime('now');
        $this->haveInDatabase('gangs', [
            'id' => $id,
            'name' => 'name' .  $id,
            'deal_type' => $type,
            'created_at' => $now->format('Y-m-d H:i:s'),
            'updated_at' => $now->format('Y-m-d H:i:s')
        ]);
    }

    public function addGangMembership($id, $memberId, $gangId, $value = 0)
    {
        $now = new \DateTime('now');
        $this->haveInDatabase('gang_memberships', [
            'id' => $id,
            'member_id' => $memberId,
            'gang_id' => $gangId,
            'value' => $value,
            'created_at' => $now->format('Y-m-d H:i:s'),
            'updated_at' => $now->format('Y-m-d H:i:s')
        ]);
    }
}
